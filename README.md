# advent of code 2019

Code for [advent of code 2019](https://adventofcode.com/2019).

Each day has a seperate file `srs/dayxx.rs` with a dedicated `run`-function.
There are also additional shared files for the intcode-computer, graphs, numbers etc.

Disclaimer: The solutions of the different days have quite varying level of quality :)

Author: Axel Dahlberg
