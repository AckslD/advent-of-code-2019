use std::convert::TryFrom;
use advent2019::toolbox::read_input;

#[derive(Debug)]
enum Technique {
    New,
    Increment(i128),
    Cut(i128),
}

impl From<&str> for Technique {
    fn from(raw_str: &str) -> Technique {
        if raw_str == "deal into new stack" {
            Technique::New
        } else if raw_str.starts_with("deal with increment ") {
            let i = raw_str.split_ascii_whitespace().last().unwrap().parse::<i128>().unwrap();
            Technique::Increment(i)
        } else if raw_str.starts_with("cut ") {
            let i = raw_str.split_ascii_whitespace().last().unwrap().parse::<i128>().unwrap();
            Technique::Cut(i)
        } else {
            panic!("Unknown technique: {}", raw_str);
        }
    }
}

fn parse_input(input: &String) -> Vec<Technique> {
    let mut techniques = Vec::new();
    for raw_str in input.split('\n') {
        if raw_str.len() == 0 {
            continue;
        }
        techniques.push(raw_str.into())
    }
    techniques
}

fn get_techniques() -> Vec<Technique> {
    let input = read_input("data/day22.txt".to_string());
    parse_input(&input)
}

const NUM_CARDS: usize = 10007;
// const NUM_CARDS: usize = 119315717514047;

struct Stack {
    cards: [u32; NUM_CARDS]
}

impl Stack {
    fn new() -> Stack {
        let mut cards: [u32; NUM_CARDS] = [0; NUM_CARDS];
        for i in 0..NUM_CARDS {
            cards[i] = i as u32;
        }
        Stack{cards}
    }

    fn do_techniques(&mut self, techniques: &Vec<Technique>) {
        for technique in techniques {
            self.do_technique(technique);
        }
    }

    fn do_technique(&mut self, technique: &Technique) {
        match technique {
            Technique::New => {self.do_new();}
            Technique::Increment(i) => {self.do_increment(*i);}
            Technique::Cut(i) => {self.do_cut(*i);}
        }
    }

    fn do_new(&mut self) {
        self.cards.reverse();
    }

    fn do_increment(&mut self, i: i128) {
        let old_cards = self.cards.clone();
        let m = usize::try_from(i).unwrap();
        for (ind, card) in old_cards.iter().enumerate() {
            self.cards[(ind*m).rem_euclid(NUM_CARDS)] = *card;
        }
    }

    fn do_cut(&mut self, i: i128) {
        let index = usize::try_from(i.rem_euclid(NUM_CARDS as i128)).unwrap();
        let old_cards = self.cards.clone();
        let (left, right) = old_cards.split_at(index);
        for (ind, card) in right.iter().enumerate() {
            self.cards[ind] = *card;
        }
        for (ind, card) in left.iter().enumerate() {
            self.cards[ind+right.len()] = *card;
        }
    }
}

fn problem_1() {
    let techniques = get_techniques();
    let mut stack = Stack::new();
    stack.do_techniques(&techniques);
    // println!("{}", stack.cards.binary_search(&2019).unwrap());
    // println!("{}", stack.cards[2018]);
    // println!("{:?}", stack.cards);
    for (i, card) in stack.cards.iter().enumerate() {
        if *card == 2019 {
            println!("{}", i);
        }
        // println!("{}", card);
    }
}

fn shuffle(start_index: i128, techniques: &Vec<Technique>) -> i128 {
    let mut index: i128 = start_index;
    for technique in techniques {
        // println!("{}", index);
        match technique {
            Technique::New => {index = (-index-1).rem_euclid(NUM_CARDS as i128);}
            Technique::Increment(i) => {index = (index * i).rem_euclid(NUM_CARDS as i128)}
            Technique::Cut(i) => {index = (index - i).rem_euclid(NUM_CARDS as i128);}
        }
    }
    index
    // println!("{}", index);
}

fn to_expression(techniques: &Vec<Technique>) -> String {
    let mut expr = "i".to_string();
    for technique in techniques {
        expr = to_expression_single(&expr, technique);
    }
    expr
}

fn to_expression_single(input: &String, technique: &Technique) -> String {
    match technique {
        Technique::New => {format!("N-({})-1", input)}
        Technique::Increment(i) => {format!("({})*{}", input, i)}
        Technique::Cut(i) => {format!("{}-({})", input, i)}
    }
}

fn geometric_mod(a: i128, n: i128, m: i128) -> i128 {
    // println!("a = {}, n = {}, m = {}", a, n, m);
    if n < 1 {
        panic!("n = {} (<1)", n);
    }
    if n == 1 {
        return 1;
    }
    let value = if n % 2 == 0 {
        ((1+a) * geometric_mod((a*a) % m, n/2, m)) % m
    } else {
        (geometric_mod(a, n-1, m) + power_mod(a, n-1, m)) % m
    };
    // println!("value = {}", value);
    value
}

fn power_mod(a: i128, n: i128, m: i128) -> i128 {
    let mut x = 1;
    let mut y = a;
    let mut b = n;
    while b > 0 {
        if b % 2 == 1 {
            x = (x*y) % m;
        }
        y = (y*y) % m;
        b /= 2
    }
    x % m
}

fn compute_card() -> i128 {
    // Input
    // a and b found using python script external/day22.py
    let x: i128 = 2020;
    let a: i128 = 92607263647118;
    let b: i128 = 2623275703944;
    let N: i128 = 119315717514047;
    let num: i128 = 101741582076661;

    // Find the inverse numbers
    // ainv found using python pow
    let ainv: i128 = 20660784091617;
    let A: i128 = ainv;
    let B: i128 = (-ainv * b).rem_euclid(N);
    (power_mod(A, num, N) * x + B * geometric_mod(A, num, N)) % N
}

fn problem_2() {
    // let techniques = get_techniques();
    // // let expr = to_expression(&techniques);
    // // println!("{}", expr);


    // let mut index: i128 = 2019;
    // let mut shuffles = 0;
    // let mut seen_positions: HashSet<i128> = HashSet::new();
    // // loop {
    // for _ in 0..100 {
    //     index = shuffle(index, &techniques);
    //     if !seen_positions.insert(index) {
    //         // println!("Seen {} before", index);
    //         break;
    //     }
    //     shuffles += 1;
    //     if shuffles % 1000000 == 0 {
    //         println!("{}", shuffles);
    //     }
    //     // if index == 2019 {
    //     //     break;
    //     // }
    // }
    // println!("{}", index);
    // println!("shuffles = {}", shuffles);

    println!("card = {}", compute_card());
}

pub fn run() {
    problem_1();
    // problem_2();
}
