use advent2019::toolbox::read_input;
use advent2019::graph::DiGraph;

fn load_graph<'a>(input: &'a str) -> DiGraph<'a> {
    let mut graph = DiGraph::new();
    for entry in input.split_terminator("\n") {
        let nodes: Vec<_> = entry.split_terminator(")").collect();
        let start = nodes[0];
        let end = nodes[1];
        graph.add_edge(start, end);
    }

    graph
}

pub fn run() {

    // Problem 1
    // let mut graph = load_map();
    // graph.breath_first_search("COM");

    // let mut sum = 0;
    // for (_, node) in &graph.nodes {
    //     sum += node.distance_com;
    //     // println!("{} : {:?}", name, node);
    // }
    // println!("{}", sum);

    // Problem 2
    let input = read_input("data/day6.txt".to_string());
    let mut graph = load_graph(&input);
    graph.breath_first_search("SAN");

    for (name, node) in &graph.nodes {
        if *name == "YOU" {
            println!("{} : {:?}", name, node);
        }
    }
}
