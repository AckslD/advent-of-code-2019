use std::collections::HashMap;

fn get_input() -> Vec<u32> {
    // let raw_str = "12345678";
    // let raw_str = "80871224585914546619083218645595";
    // let raw_str = "19617804207202209144916044189917";
    // let raw_str = "69317163492948606335995924319873";
    // let raw_str = "03036732577212944063491565474664";
    // let raw_str = "02935109699940807407585447034323";
    // let raw_str = "03081770884921959731165446850517";
    let raw_str = "59787832768373756387231168493208357132958685401595722881580547807942982606755215622050260150447434057354351694831693219006743316964757503791265077635087624100920933728566402553345683177887856750286696687049868280429551096246424753455988979991314240464573024671106349865911282028233691096263590173174821612903373057506657412723502892841355947605851392899875273008845072145252173808893257256280602945947694349746967468068181317115464342687490991674021875199960420015509224944411706393854801616653278719131946181597488270591684407220339023716074951397669948364079227701367746309535060821396127254992669346065361442252620041911746738651422249005412940728";
    raw_str.chars().map(|d| d.to_digit(10).unwrap()).collect()
}

fn fft(input: &Vec<u32>, iterations: usize, digit: usize, cache: &mut HashMap<(usize, usize), u32>) -> u32 {
    if iterations == 0 {
        return input[digit];
    }
    let mut sum = 0;
    let mut pattern: i32;
    let i = digit;
    for j in (0..(input.len() - i)).filter(|x| (x % (4 * (i + 1)) / (i + 1)) % 2 == 0)  {
        pattern = match j % (4 * (i + 1)) / (i + 1) {
            0 => 1,
            2 => -1,
            _ => panic!("Not valid number")
        };
        let inp: u32;
            let key = (iterations - 1, j + i);
            match cache.get(&key) {
                Some(val) => {
                    inp = *val;
                }
                None => {
                    inp = fft(input, key.0, key.1, cache);
                    cache.insert(key, inp);
                }
            }
        sum += (inp as i32) * pattern;
    }
    
    (sum % 10).abs() as u32
}

fn get_digits(input: &Vec<u32>, iterations: usize, offset: usize) -> Vec<u32> {
    let mut output = Vec::new();
    for i in 0..(input.len() - offset) {
        output.push(input[i + offset]);
    }
    let n = output.len();
    for _ in 0..iterations {
        for i in 1..n {
            output[n - i - 1] += output[n - i];
            output[n - i - 1] %= 10;
        }
    }
    output
}

pub fn run() {
    let input = get_input();
    // Problem 1
    let mut cache = HashMap::new();
    for j in 0..8 {
        let digit = fft(&input, 100, j, &mut cache);
        print!("{}", digit);
    }

    // Problem 2
    let mut rep_input: Vec<u32> = Vec::new();
    for _ in 0..10000 {
        for x in &input {
            rep_input.push(*x);
        }
    }
    println!("{}", rep_input.len());
    let offset: usize = input[0..7].iter().map(|d| d.to_string()).collect::<Vec<String>>().join("").parse().unwrap();
    println!("{}", offset);
    println!("{}", offset as f64 / (input.len() * 10000) as f64);
    println!("Computing digits");
    let output = get_digits(&rep_input, 100, offset);
    for d in output[0..8].iter() {
        print!("{}", d);
    }
}
