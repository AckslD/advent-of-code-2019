use std::io;
use std::fmt;
use std::collections::{VecDeque, HashMap};
use std::{thread, time};
use device_query::{DeviceQuery, DeviceState, Keycode};
use advent2019::intcode::{Program, ProgramState, execute_program, Memory};
use advent2019::graph::{DiGraph, Node};

#[derive(PartialEq, Eq, Clone, Copy, std::hash::Hash, Debug)]
struct Position {
    x: i64,
    y: i64,
}

impl<'a> Position {
    fn to_tuple(&self) -> (i64, i64) {
        (self.x, self.y)
    }

    // fn to_string(&self) -> &'a str {
    //     format!("{},{}", self.x, self.y)
    // }
}

#[derive(Debug)]
enum NodeState {
    Marked,
    Blank,
}

#[derive(Debug)]
pub struct DistNode {
    state: NodeState,
    distance_com: i64,
}

impl DistNode {
    fn new() -> DistNode {
        DistNode{state: NodeState::Blank, distance_com: 0}
    }

    fn mark(&mut self) {
        self.state = NodeState::Marked
    }

    pub fn get_distance(&self) -> i64 {
        self.distance_com
    }
}

impl From<()> for DistNode {
    fn from(_x: ()) -> DistNode {
        DistNode::new()
    }
}

impl Node for DistNode {
    fn update_state(&mut self, from_node: &DistNode) -> bool {
        match self.state {
            NodeState::Marked => {false}
            NodeState::Blank => {
                self.mark();
                self.distance_com = from_node.distance_com + 1;
                true
            }
        }
    }

    fn start_state(&mut self) {
        self.mark();
    }
}

type Graph = DiGraph<Position, DistNode, ()>;

struct Robot {
    pos: Position,
}

impl Robot {
    fn new() -> Robot {
        Robot{pos: Position{x: 0, y: 0}}
    }
}

#[derive(PartialEq, Eq, Clone, Copy)]
enum Tile {
    Empty,
    Explored,
    Robot,
    Wall,
    Goal,
}

struct Board {
    board: HashMap<(i64,i64),Tile>,
}

impl Board {
    pub fn new() -> Board {
        Board{board: HashMap::new()}
    }

    pub fn read(&mut self, x: i64, y:i64) -> Tile {
        *self.board.entry((x, y)).or_insert(Tile::Empty)
    }

    pub fn write(&mut self, x: i64, y:i64, value: Tile) {
        self.board.insert((x, y), value);
    }

    pub fn range(&self) -> (i64, i64, i64, i64) {
        // return (-20, 20, -20, 20);
        if self.board.len() == 0 {
            return (0, 0, 0, 0);
        }
        let mut xmin = -20;
        let mut xmax = 20;
        let mut ymin = -20;
        let mut ymax = 20;
        for ((x, y), _) in self.board.iter() {
            if *x < xmin {
                xmin = *x;
            }
            if *x > xmax {
                xmax = *x;
            }
            if *y < ymin {
                ymin = *y;
            }
            if *y > ymax {
                ymax = *y;
            }
        }

        (xmin, xmax, ymin, ymax)
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output = String::new();
        let (xmin, xmax, ymin, ymax) = self.range();
        for y in (-ymax)..(-ymin + 1) {
            for x in xmin..(xmax + 1) {
                match self.board.get(&(x, -y)) {
                    Some(c) => {
                        match c {
                            Tile::Empty => output.push(' '),
                            Tile::Explored => output.push('.'),
                            Tile::Wall => output.push('#'),
                            Tile::Robot => output.push('o'),
                            Tile::Goal => output.push('x'),
                        };
                    }
                    None => {output.push(' ')}
                }
            }
            output.push('\n');
        }
        write!(f, "{}", output)
    }
}

fn get_initial_program() -> Program {
    let raw_program: Vec<i64>;
    raw_program = vec!(3,1033,1008,1033,1,1032,1005,1032,31,1008,1033,2,1032,1005,1032,58,1008,1033,3,1032,1005,1032,81,1008,1033,4,1032,1005,1032,104,99,1001,1034,0,1039,1002,1036,1,1041,1001,1035,-1,1040,1008,1038,0,1043,102,-1,1043,1032,1,1037,1032,1042,1105,1,124,1001,1034,0,1039,1001,1036,0,1041,1001,1035,1,1040,1008,1038,0,1043,1,1037,1038,1042,1106,0,124,1001,1034,-1,1039,1008,1036,0,1041,101,0,1035,1040,1001,1038,0,1043,102,1,1037,1042,1105,1,124,1001,1034,1,1039,1008,1036,0,1041,1002,1035,1,1040,101,0,1038,1043,101,0,1037,1042,1006,1039,217,1006,1040,217,1008,1039,40,1032,1005,1032,217,1008,1040,40,1032,1005,1032,217,1008,1039,5,1032,1006,1032,165,1008,1040,35,1032,1006,1032,165,1102,1,2,1044,1106,0,224,2,1041,1043,1032,1006,1032,179,1102,1,1,1044,1105,1,224,1,1041,1043,1032,1006,1032,217,1,1042,1043,1032,1001,1032,-1,1032,1002,1032,39,1032,1,1032,1039,1032,101,-1,1032,1032,101,252,1032,211,1007,0,44,1044,1105,1,224,1102,0,1,1044,1106,0,224,1006,1044,247,1002,1039,1,1034,1001,1040,0,1035,1001,1041,0,1036,1002,1043,1,1038,102,1,1042,1037,4,1044,1105,1,0,5,26,24,17,68,40,71,9,36,46,67,39,48,8,20,23,12,47,28,13,47,2,68,17,71,31,63,31,83,14,78,31,8,33,30,63,30,5,7,11,91,97,17,84,23,37,46,6,14,59,1,76,41,63,85,83,86,63,33,13,50,17,37,16,59,8,7,35,71,9,23,67,46,62,58,38,76,3,71,43,17,64,29,30,72,91,17,70,21,15,76,31,89,20,38,27,65,53,60,34,90,99,56,15,45,57,8,52,70,36,15,79,32,35,83,78,10,3,90,16,74,14,84,43,20,81,91,25,71,83,24,31,92,72,34,59,27,78,6,31,14,31,76,9,80,63,35,40,92,12,84,65,41,27,82,10,7,56,25,70,4,98,16,37,65,46,78,11,97,20,16,95,98,24,31,3,57,74,42,99,36,34,74,10,81,46,43,97,2,24,61,55,13,96,41,41,46,14,64,2,46,94,53,3,3,81,37,85,7,54,29,90,22,75,47,20,26,86,69,53,89,17,2,55,13,85,99,90,2,48,29,66,55,31,19,39,59,56,98,28,38,10,46,10,62,20,63,18,53,97,9,32,6,46,3,91,24,6,62,30,73,26,24,50,3,16,78,3,34,50,8,18,40,65,64,21,28,30,87,45,99,8,21,77,40,73,38,56,12,86,64,43,61,89,4,55,47,28,14,8,99,52,51,40,82,26,19,68,17,53,70,5,14,22,64,69,84,14,69,2,80,18,79,5,66,18,34,48,31,34,54,50,8,33,73,38,52,94,71,7,31,94,31,93,66,82,39,40,42,80,91,70,10,6,50,35,96,13,7,89,22,58,30,24,85,81,88,55,7,58,38,91,55,11,35,84,28,87,26,78,48,66,11,88,8,18,68,55,38,6,1,57,60,1,8,99,58,21,29,88,32,32,57,72,8,20,45,5,91,39,51,59,82,29,52,37,33,49,5,28,38,17,6,58,67,11,72,51,42,4,3,12,94,84,25,31,72,32,89,49,4,23,57,49,27,38,50,30,23,15,80,4,12,67,14,48,76,91,58,11,63,37,95,1,15,22,84,8,23,87,61,32,78,87,7,47,1,81,31,84,91,21,19,68,6,87,3,72,43,60,23,67,42,40,62,9,86,33,84,69,24,97,37,49,24,67,2,16,52,3,42,49,3,95,84,61,8,40,79,10,74,51,6,77,63,1,66,7,55,24,80,68,17,30,47,54,30,77,40,99,18,85,99,85,2,27,18,33,54,99,27,5,64,39,22,66,12,71,29,26,35,49,13,41,22,76,30,70,30,75,34,7,5,62,1,23,61,43,90,24,91,40,42,75,48,40,91,39,46,38,56,17,28,51,56,7,51,40,56,22,87,43,99,6,58,93,35,47,83,10,57,55,68,34,68,93,28,55,11,3,53,80,9,41,42,50,95,7,4,84,10,91,33,12,99,98,60,76,73,24,70,46,72,27,36,62,27,25,43,59,39,9,95,72,9,17,79,36,52,52,22,4,55,57,16,19,65,62,83,11,76,73,37,89,21,86,6,88,17,93,1,59,8,48,73,90,96,10,85,46,12,99,16,16,76,4,2,2,45,62,30,12,14,72,60,9,19,71,43,41,36,99,69,38,1,1,48,32,33,83,26,15,51,19,31,71,92,8,49,34,87,32,80,73,28,65,95,7,8,85,12,63,22,83,8,70,1,82,96,59,29,95,43,59,72,68,38,48,11,87,54,90,11,93,30,63,12,96,41,64,21,89,24,94,73,79,18,55,40,95,0,0,21,21,1,10,1,0,0,0,0,0,0);
    let output: VecDeque<i64> = VecDeque::new();
    Program{
        memory: Memory{data: raw_program},
        input: VecDeque::new(),
        output,
        instr_pointer: 0,
        state: ProgramState::Running,
        rel_base: 0,
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
enum Input {
    North = 1,
    South = 2,
    West = 3,
    East = 4,
    // None,
}

impl Input {
    fn opposite(&self) -> Input {
        match self {
            Input::North => Input::South,
            Input::South => Input::North,
            Input::West => Input::East,
            Input::East => Input::West,
        }
    }
}

#[derive(PartialEq, Clone, Copy, Debug)]
enum Output {
    Wall = 0,
    Move = 1,
    Finish = 2,
}

const INPUTS: [Input; 4] = [Input::North, Input::South, Input::West, Input::East];

fn get_input(_device_state: &DeviceState, robot: &Robot, pos_datas: &mut HashMap<(i64, i64), PosData>) -> Input {
    let pos_data = pos_datas.entry(robot.pos.to_tuple()).or_insert(PosData::new());
    // println!("Pos data is {:?}", pos_data);
    for input in INPUTS.iter() {
        match pos_data.explored.get(input) {
            Some(_) => {}
            None => {
                // pos_data.explored.insert(*input, true);
                return *input;
            }
        }
    }
    match pos_data.previous {
        Some(inp) => inp,
        None => panic!("Returned to starting position"),
    }

    
    // let mut input = Input::None;
    // while input == Input::None {
    //     let keys: Vec<Keycode> = device_state.get_keys();
    //     for key in keys {
    //         match key {
    //             Keycode::H => input = Input::West,
    //             Keycode::J => input = Input::South,
    //             Keycode::K => input = Input::North,
    //             Keycode::L => input = Input::East,
    //             _ => {}
    //         };
    //     }
    //     thread::sleep(time::Duration::from_millis(50));
    // }

    // input
    // let mut inp_str: String;
    // let mut input = Input::None;
    // while input == Input::None {
    //     inp_str = String::new();
    //     match io::stdin().read_line(&mut inp_str) {
    //         Ok(_) => {
    //             match inp_str.as_ref() {
    //                 "h\n" => {input = Input::West} // west
    //                 "l\n" => {input = Input::East} // east
    //                 "j\n" => {input = Input::South} // south
    //                 "k\n" => {input = Input::North} // north
    //                 _ => {}
    //             }
    //         }
    //         Err(error) => println!("error: {}", error),
    //     }
    // }

    // input
}

fn get_output(program: &mut Program) -> Output {
    let output = program.output.pop_front().expect("No more output");
    match output {
        0 => Output::Wall,
        1 => Output::Move,
        2 => Output::Finish,
        // 2 => panic!("Finish"),
        _ => panic!("Unknown output {}", output),
    }
}

fn update_state(
    board: &mut Board,
    robot: &mut Robot,
    pos_datas: &mut HashMap<(i64, i64), PosData>,
    graph: &mut Graph,
    input: Input,
    output: Output,
    ) -> u64 {

    let mut step = 0;
    // pos_datas.get_mut(&robot.pos.to_tuple()).unwrap().explored.insert(input, true);
    if output == Output::Wall {
        pos_datas.get_mut(&robot.pos.to_tuple()).unwrap().explored.insert(input, true);
        match input {
            Input::North => board.write(robot.pos.x, robot.pos.y + 1, Tile::Wall),
            Input::South => board.write(robot.pos.x, robot.pos.y - 1, Tile::Wall),
            Input::West => board.write(robot.pos.x - 1, robot.pos.y, Tile::Wall),
            Input::East => board.write(robot.pos.x + 1, robot.pos.y, Tile::Wall),
            // Input::None => panic!()
        };
    } else {
        // TODO Update pos data
        // let prev_pos = Position{x: robot.pos.x, y: robot.pos.y};
        // let prev_pos_data = pos_datas.get(&robot.pos.to_tuple()).unwrap();
        // let prev_dist = prev_pos_data.distance;
        let prev_pos = robot.pos;
        let prev_dist = pos_datas.get(&robot.pos.to_tuple()).unwrap().distance;
        let prev_previous = pos_datas.get(&robot.pos.to_tuple()).unwrap().previous;
        // if pos_datas.get(&robot.pos.to_tuple()).unwrap().previous == input {
        //     pos_datas.get_mut(&robot.pos.to_tuple()).unwrap().explored.insert(input, true);
        // }
        step += 1;
        if board.read(robot.pos.x, robot.pos.y) == Tile::Robot {
            board.write(robot.pos.x, robot.pos.y, Tile::Explored);
        }
        match input {
            Input::North => robot.pos.y += 1,
            Input::South => robot.pos.y -= 1,
            Input::West => robot.pos.x -= 1,
            Input::East => robot.pos.x += 1,
            // Input::None => panic!()
        };
        let new_pos = robot.pos;
        graph.add_edge(prev_pos, (), new_pos, ());
        board.write(robot.pos.x, robot.pos.y, Tile::Robot);

        let previous = input.opposite();
        let new_pos_data = pos_datas.entry(robot.pos.to_tuple()).or_insert(PosData::new());
        match new_pos_data.previous {
            Some(_) => {}
            None => {
                new_pos_data.previous = Some(previous);
                new_pos_data.explored.insert(previous, true);
            }
        }
        match prev_previous {
            Some(prev) => {
                if prev == input {
                    // prev_pos_data.explored.insert(input, true);
                    new_pos_data.explored.insert(previous, true);
                }
            }
            None => {}
        }
        if new_pos_data.distance == 0 {
            new_pos_data.distance = prev_dist + 1;
        } else if new_pos_data.distance > prev_dist + 1 {
            new_pos_data.distance = prev_dist + 1;
        }
    }
    if output == Output::Finish {
        board.write(robot.pos.x, robot.pos.y, Tile::Goal);
    }

    step
}

// struct Direction {
//     Left,

struct PosData {
    distance: u64,
    previous: Option<Input>,
    explored: HashMap<Input, bool>,
}

impl PosData {
    fn new() -> PosData {
        PosData{
            distance: 0,
            previous: None,
            explored: HashMap::new(),
        }
    }
}

fn run_program(board: &mut Board) {
    let program = &mut get_initial_program();
    let mut robot = Robot::new();
    let mut steps_taken = 0;
    let device_state = DeviceState::new();
    let mut pos_datas: HashMap<(i64, i64), PosData> = HashMap::new();
    let mut graph = Graph::new();
    let start_pos = PosData{
        distance: 0,
        previous: Option::None,
        explored: HashMap::new(),
    };
    pos_datas.insert((0, 0), start_pos);
    let mut finish_pos: Option<Position> = None;
    loop {
        println!("{}", board);
        println!("Steps: {}", steps_taken);
        let input = get_input(&device_state, &robot, &mut pos_datas);
        // println!("Input is {:?}", input);
        // if input != Input::None {
        program.input.push_back(input as i64);
        // }
        execute_program(program, ProgramState::Waiting);
        if program.state == ProgramState::Finished {
            return
        }
        let output = get_output(program);
        // println!("Output is {:?}", output);
        let step = update_state(board, &mut robot, &mut pos_datas, &mut graph, input, output);
        println!("Distance: {}", pos_datas.get(&robot.pos.to_tuple()).unwrap().distance);
        if output == Output::Finish {
            finish_pos = Some(robot.pos);
            // break;
        }
        steps_taken += step;
        thread::sleep(time::Duration::from_millis(5));
        if robot.pos.to_tuple() == (0, 0) && steps_taken > 1600 {
            break;
        }
    }
    println!("Final distance: {}", pos_datas.get(&finish_pos.unwrap().to_tuple()).unwrap().distance);
    let lsp = find_longest_shortest_path(&finish_pos.unwrap(), &mut graph);
    println!("Longest shortest path is: {}", lsp);
    // println!("{:#?}", graph);
}

fn find_longest_shortest_path(start: &Position, graph: &mut Graph) -> i64 {
    graph.breath_first_search(start);
    let mut lsp = 0;
    for (_, node) in &graph.nodes {
        let dist = node.borrow().get_distance();
        if dist > lsp {
            lsp = dist;
        }
    }

    lsp
}

pub fn run() {
    // loop {
    //     let input = get_input();
    //     println!("{:?}", input);
    // }
    let mut board = Board::new();
    run_program(&mut board);
    // run_program(&mut board);
}
