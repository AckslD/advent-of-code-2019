use std::str::FromStr;
use std::string::ParseError;
use std::collections::HashMap;
use advent2019::toolbox::read_input;

#[derive(Debug)]
struct Chemical {
    name: String,
    quantity: u64,
}

impl FromStr for Chemical {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split(' ').collect();
        assert_eq!(split.len(), 2);
        let quantity: u64 = split[0].parse().unwrap();
        let name = split[1].to_string();

        Ok(Chemical{name, quantity})
    }
}

#[derive(Debug)]
struct Reaction {
    inputs: Vec<Chemical>,
    output: Chemical,
}

impl FromStr for Reaction {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let split: Vec<&str> = s.split(" => ").collect();

        let output: Chemical = split[1].parse().unwrap();

        let mut inputs: Vec<Chemical> = Vec::new();
        let inputs_str = split[0];
        for input_str in inputs_str.split(", ") {
            let input: Chemical = input_str.parse().unwrap();
            inputs.push(input);
        }

        Ok(Reaction{inputs, output})
    }
}

struct Factory {
    reactions: HashMap<String, Reaction>,
}

impl Factory {
    fn new() -> Factory {
        Factory{reactions: HashMap::new()}
    }

    fn add(&mut self, reaction: Reaction) {
        self.reactions.insert(reaction.output.name.to_string(), reaction);
    }

    fn get(&self, name: &str) -> Option<&Reaction> {
        self.reactions.get(name)
    }
}

impl FromStr for Factory {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut factory = Factory::new();
        for reaction_str in s.split_terminator('\n') {
            let reaction: Reaction = reaction_str.parse().unwrap();
            factory.add(reaction);
        }

        Ok(factory)
    }
}

#[derive(Debug)]
struct ReactionUsage {
    reactions_needed: u64,
    outputs_needed: u64,
}

impl ReactionUsage {
    fn new() -> ReactionUsage {
        ReactionUsage{reactions_needed: 0, outputs_needed: 0}
    }
}

fn compute_reactions_needed(
    usage: &mut HashMap<String, ReactionUsage>,
    reaction: &Reaction,
    outputs_needed: u64,
    ) -> u64 {

    let output_name = (&reaction.output.name).to_string();
    let mut reaction_usage = usage.entry(output_name).or_insert(ReactionUsage::new());

    let current_reactions_needed = reaction_usage.reactions_needed;
    reaction_usage.outputs_needed += outputs_needed;
    let total_outputs_needed = reaction_usage.outputs_needed;
    reaction_usage.reactions_needed = (total_outputs_needed + reaction.output.quantity - 1) / reaction.output.quantity;

    reaction_usage.reactions_needed - current_reactions_needed
}

fn reactions_needed(
    factory: &Factory,
    output: &str,
    quantity: u64,
    usage: &mut HashMap<String, ReactionUsage>,
    ) {

    if output == "ORE" {
        return
    }
    let reaction = factory.get(&output).unwrap();
    let extra_reactions_needed = compute_reactions_needed(usage, reaction, quantity);
    for input in &reaction.inputs {
        let input_quantity_needed = extra_reactions_needed * input.quantity;
        reactions_needed(factory, &input.name, input_quantity_needed, usage);
    }

}

fn compute_ores(factory: &Factory, output: &str, quantity: u64) -> u64 {
    let mut usage = HashMap::new();
    reactions_needed(factory, output, quantity, &mut usage);
    let mut num_ores = 0;
    for (name, reaction_usage) in usage.iter() {
        let reaction = factory.get(name).unwrap();
        for input in &reaction.inputs {
            if input.name == "ORE" {
                num_ores += reaction_usage.reactions_needed * input.quantity;
            }
        }
    }

    num_ores
}

fn compute_fuel(factory: &Factory) -> u64 {
	let mut i = 0;
	let mut max = 0;
	loop {
    // for i in 0..10 {
		max = 10u64.pow(i);
		if enough_ores(factory, max) == 1 {
			break;
		}
        i += 1;
	}
	find_fuel(factory, 1, max)
}

fn find_fuel(factory: &Factory, min: u64, max: u64) -> u64 {
    let middle = (min + max) / 2;
    // println!("{}, {}, {}", min, middle, max);
    if middle == min {
        match enough_ores(factory, max) {
            1 => return min,
            0 => return max,
            _ => panic!("something went wrong"),
        };
        // if enough_ores(factory, m) == 0 {
        //     return min;
        // } else if enough_ores(factory, max) == 0 {
        //     return max;
        // } else {
        //     panic!("something went wrong");
        // }
    }
    match enough_ores(factory, middle) {
        0 => middle,
        1 => find_fuel(factory, min, middle),
        -1 => find_fuel(factory, middle, max),
        _ => panic!("Wrong output from enough ores"),
    }
    // while min != max {
    //     match compute_ores(factory, "FUEL", min)
    // }
}

fn enough_ores(factory: &Factory, fuel: u64) -> i8 {
	let num_ores = compute_ores(factory, "FUEL", fuel);
	if num_ores == 1000000000000 {
		0
	} else if num_ores > 1000000000000 {
		1
	} else {
		-1
	}
}

pub fn run() {
    let raw_str = read_input("data/day14.txt".to_string());
    let factory: Factory = raw_str.parse().unwrap();

	// Problem 1
    let num_ores = compute_ores(&factory, "FUEL", 1);
    println!("{}", num_ores);

	// Problem 2
    let fuel = compute_fuel(&factory);
    println!("{}", fuel);
}
