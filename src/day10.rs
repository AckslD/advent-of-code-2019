use std::ops;
use std::cmp::{PartialEq,Ordering};
use std::f64::consts::PI;
use advent2019::graph::DiGraph;
use advent2019::numbers::gcd;
use advent2019::toolbox::read_input;

#[derive(Debug)]
struct Position {
    x: i64,
    y: i64,
    name: String,
}

impl PartialEq for Position {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y
    }
}

impl ops::Add<Position> for Position {
    type Output = Position;

    fn add(self, rhs: Position) -> Position {
        Position::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl ops::Sub<Position> for Position {
    type Output = Position;

    fn sub(self, rhs: Position) -> Position {
        Position::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl ops::Sub<&Position> for &Position {
    type Output = Position;

    fn sub(self, rhs: &Position) -> Position {
        Position::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl ops::Mul<i64> for Position {
    type Output = Position;

    fn mul(self, rhs: i64) -> Position {
        Position::new(self.x * rhs, self.y * rhs)
    }
}

impl ops::Div<i64> for Position {
    type Output = Position;

    fn div(self, rhs: i64) -> Position {
        Position::new(self.x / rhs, self.y / rhs)
    }
}

impl ops::Div<i64> for &Position {
    type Output = Position;

    fn div(self, rhs: i64) -> Position {
        Position::new(self.x / rhs, self.y / rhs)
    }
}

impl Position {
    fn new(x: i64, y: i64) -> Position {
        Position{x, y, name: format!("{},{}", x, y)}
    }

    fn decompose(&self) -> (i64, Position) {
        let multiple = gcd(self.x, self.y);
        (multiple, self / multiple)
    }

    fn angle(&self, base: &Position) -> f64 {
        let diff = self - base;
        let theta = (diff.x as f64).atan2(-diff.y as f64);
        if theta < 0. {
            2. * PI + theta
        } else {
            theta
        }
    }

    fn copy(&self) -> Position {
        Position::new(self.x, self.y)
    }

    fn to_string<'a>(&'a self) -> &'a str {
        &self.name
    }
}

#[derive(Debug)]
struct Asteroid<'a> {
    pos: &'a Position,
    angle: f64,
}

fn parse_input(filepath: String) -> Vec<Vec<char>> {
    let raw_str = read_input(filepath);

    let mut rows = Vec::new();

    for row_str in raw_str.split_terminator('\n') {
        let row = row_str.chars().collect();
        rows.push(row);
    }

    rows
}

fn get_positions(filepath: String) -> Vec<Position> {
    let rows = parse_input(filepath);
    let mut positions: Vec<Position> = Vec::new();
    for y in 0..rows.len() {
        let row = &rows[y];
        for x in 0..row.len() {
            if row[x] == '#' {
                positions.push(Position::new(x as i64, y as i64));
            }
        }
    }

    positions
}

fn construct_graph<'a>(positions: &'a Vec<Position>) -> DiGraph<'a> {
    let mut graph = DiGraph::new();
    // for position in positions {
    //     graph.add_node(position.to_string());
    // }

    for i in 0..positions.len() {
        for j in 0..positions.len() {
            let pos1 = &positions[i];
            let pos2 = &positions[j];
            if in_sight(positions, pos1, pos2) {
                graph.add_edge(pos1.to_string(), pos2.to_string());
            }
        }
    }

    graph

}

fn in_sight(positions: &Vec<Position>, pos1: &Position, pos2: &Position) -> bool {
    if pos1 == pos2 {
        return false;
    }
    let diff = pos2 - pos1;
    let (multiple, base) = diff.decompose();
    // println!("m = {}, b = {:?}", multiple, base);
    for other in positions {
        if !(other == pos1 || other == pos2) {
            let other_diff = other - pos1;
            let (other_multiple, other_base) = other_diff.decompose();
            if base == other_base {
                if other_multiple * multiple > 1 {
                    if other_multiple.abs() <= multiple.abs() {
                        // println!("k = {}", k);
                        // println!("other mul = {}", other_multiple);
                        return false;
                    }
                }
            }
        }
    }

    true
}

fn find_best_pos<'a>(graph: &DiGraph, positions: &'a Vec<Position>) -> (usize, Option<&'a Position>) {
    let mut max_degree = 0;
    let mut max_pos = None;
    for position in positions {
        let deg = graph.degree(&format!("{},{}", position.x, position.y)).unwrap();
        if deg > max_degree {
            max_degree = deg;
            max_pos = Some(position);
        }
    }

    (max_degree, max_pos)
}

pub fn run() {
    // let p = Position::new(3, 6);
    // let (m, base) = p.decompose();
    // println!("{}, {:?}", m, base);
    // println!("{:?}", p);

    // Problem 1
    let positions = get_positions("data/day10.txt".to_string());
    //println!("{}", gcd(3, -6));
    // println!("{:?}", positions);
    //let i = 5;
    //let j = 4;
    //println!("{:?}", positions[i]);
    //println!("{:?}", positions[j]);
    //// println!("{}", positions.len());
    ////
    //println!("{}", in_sight(&positions, i, j));

    let graph = construct_graph(&positions);
    // println!("{:#?}", graph);

    // for y in 0..5 {
    //     for x in 0..5 {
    //         match graph.degree(&format!("{},{}", x, y)) {
    //             Some(deg) => print!("{}", deg),
    //             None => print!("."),
    //         };
    //     }
    //     print!("\n");
    // }

    let (max_degree, max_pos) = find_best_pos(&graph, &positions);
    println!("max = {} at pos {}", max_degree, max_pos.unwrap().to_string());

    // Problem 2
    let base = max_pos.unwrap();
    let mut asteroids: Vec<Asteroid> = Vec::new();
    // let mut angels: Vec<f64> = Vec::new();
    for position in &positions {
        let asteroid = Asteroid{pos: position, angle: position.angle(base)};
        asteroids.push(asteroid);
        // angels.push(position.angle(base));
    }

    asteroids.sort_by(|a, b| a.angle.partial_cmp(&b.angle).unwrap_or(Ordering::Equal));
    // for i in 0..asteroids.len() {
    // for i in 0..30 {
    //     println!("{:?}", asteroids[i]);
    // }
    // println!("{:#?}", asteroids);

    let mut count = 0;
    let max_count = 1000;
    while asteroids.len() > 1 && count < max_count {
        let mut current_positions = Vec::new();
        for i in 0..asteroids.len() {
            current_positions.push(asteroids[i].pos.copy());
        }
        let mut i = 0;
        let mut angle = -1.;
        while i < asteroids.len() && count < max_count {
        // for asteroid in &asteroids {
            // println!("i = {}", i);
            let asteroid = &asteroids[i];
            let start = base;
            let end = asteroid.pos;
            // if in_sight(&current_positions, start, end) {
            if asteroid.angle > angle && in_sight(&current_positions, start, end) {
                count += 1;
                // println!("in sight {:?} and {:?}", start, end);
                let removed = asteroids.remove(i);
                angle = removed.angle;
                current_positions.remove(i);
                if count == 200 {
                    println!("{} : {:?}", count, removed);
                    println!("Answer: {}", removed.pos.x * 100 + removed.pos.y);
                }
                // break;
            } else {
                i += 1;
            }
        }
    }
}
