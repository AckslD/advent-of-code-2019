use std::fmt;
use std::convert::{TryFrom, TryInto};
use std::collections::{HashMap, HashSet};
use advent2019::graph;
use advent2019::toolbox;
use advent2019::grid;

#[derive(Clone, Copy, PartialEq, Debug, Eq, Hash)]
enum Tile {
    Wall,
    Empty,
    Key(char),
    Door(char),
    Start,
    SplitStart(char),
}

impl TryFrom<char> for Tile {
    type Error = &'static str;

    fn try_from(c: char) -> Result<Self, Self::Error> {
        if c.is_ascii_lowercase() {
            Ok(Tile::Key(c))
        } else if c.is_ascii_uppercase() {
            Ok(Tile::Door(c))
        } else if c == '.' {
            Ok(Tile::Empty)
        } else if c == '#' {
            Ok(Tile::Wall)
        } else if c == '@' {
            Ok(Tile::Start)
        } else if c.is_digit(10) {
            Ok(Tile::SplitStart(c))
        } else {
            Err("Unknown tile")
        }
    }
}

impl Into<char> for Tile {
    fn into(self) -> char {
        match self {
            Tile::Wall => {'#'}
            Tile::Empty => {'.'}
            Tile::Key(c) | Tile::Door(c) | Tile::SplitStart(c) => {c}
            Tile::Start => {'@'}
        }
    }
}

#[derive(Debug)]
struct FullNode {
    tile: Tile,
    marked: bool,
    distance: Option<u32>,
}

impl FullNode {
    fn new(c: char) -> FullNode {
        FullNode{
            tile: Tile::try_from(c).unwrap(),
            marked: false,
            distance: None,
        }
    }
}

impl fmt::Display for FullNode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "FullNode({})", Into::<char>::into(self.tile))
    }
}

impl From<char> for FullNode {
    fn from(c: char) -> Self {
        FullNode::new(c)
    }
}

impl graph::Node<grid::Position, (), FullBranchState> for FullNode {
    fn get_new_state(&mut self, from_id: &grid::Position, from_node: &Self, state: &FullBranchState, edge_data: &()) -> FullBranchState {
        let steps = state.steps + 1;
        let status = if self.marked {
            graph::BFSStatus::Discard
        } else {
            self.distance = Some(steps);
            self.marked = true;
            match self.tile {
                Tile::Door(_) => {graph::BFSStatus::Discard}
                Tile::Key(_) | Tile::Start | Tile::Empty | Tile::SplitStart(_) => {graph::BFSStatus::Process}
                Tile::Wall => {panic!("Wall in graph");}
            }
        };
        FullBranchState{
            status,
            steps,
        }
    }
}

impl grid::TileNode for FullNode {
    fn get_tile(&self) -> char {
        self.tile.into()
    }
}

struct FullBranchState {
    status: graph::BFSStatus,
    steps: u32,
}

impl FullBranchState {
    fn new() -> FullBranchState {
        FullBranchState{
            status: graph::BFSStatus::Process,
            steps: 0,
        }
    }
}

impl graph::State<grid::Position, FullNode> for FullBranchState {
    fn get_status(&self) -> graph::BFSStatus {
        self.status
    }
    fn get_steps(&self) -> u32 {
        self.steps
    }
}

#[derive(Debug)]
struct Node {
}

impl Node {
    fn new() -> Node {
        Node{}
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Node()")
    }
}

impl From<()> for Node {
    fn from(_: ()) -> Self {
        Node::new()
    }
}

type FullNodeData = char;
type FullGraph = grid::GridGraph<FullNode, (), FullNodeData, FullBranchState>;

type EdgeData = u32;
type NodeData = ();
type ReducedGraph = graph::DiGraph<Tile, Node, EdgeData, NodeData, ()>;

fn get_input(file: &'static str) -> String {
    toolbox::read_input(file.to_string())
}

fn load_full_graph(file: &'static str) -> FullGraph {
    let raw_str = get_input(file);
    let mut row = 0;
    let mut col = 0;
    let mut empty_graph = FullGraph::new();
    for c in raw_str.chars() {
        if c == '\n' {
            col = 0;
            row += 1;
        } else {
            if Tile::try_from(c).unwrap() != Tile::Wall {
                let pos = grid::Position{x: col, y: row};
                empty_graph.add_node(pos, c);
            }
            col += 1
        }
    }

    let mut graph = FullGraph::new(); // NOTE not so nice to create another one but simple for now
    for (pos, node) in &empty_graph.nodes {
        for neigh_pos_res in pos.surrounding() {
            if let Ok(neigh_pos) = neigh_pos_res {
                if let Some(neigh) = empty_graph.nodes.get(&neigh_pos) {
                    graph.add_node(*pos, node.borrow().tile.into());
                    graph.add_node(neigh_pos, neigh.borrow().tile.into());
                    graph.add_edge(*pos, neigh_pos, ());
                }
            }
        }
    }

    graph
}

fn load_reduced_graph(file: &'static str) -> ReducedGraph {
    let mut full_graph = load_full_graph(file);
    println!("{}", full_graph);
    let mut keys_pos: HashMap<grid::Position, char> = HashMap::new();
    let mut doors_pos: HashMap<grid::Position, char> = HashMap::new();
    for (pos, node_ref) in &full_graph.nodes {
        let node = node_ref.borrow();
        match node.tile {
            Tile::Key(c) => {keys_pos.insert(*pos, c);}
            Tile::Door(c) => {doors_pos.insert(*pos, c);}
            Tile::Start => {keys_pos.insert(*pos, '@');}  // Treat as key for now
            Tile::SplitStart(c) => {keys_pos.insert(*pos, c);}  // Treat as key for now
            _ => {}
        }
    }
    let mut reduced_graph = ReducedGraph::new();
    let mut all_pos: HashMap<grid::Position, char> = HashMap::new();
    for (pos, c) in &keys_pos {
        all_pos.insert(*pos, *c);
    }
    for (pos, c) in &doors_pos {
        all_pos.insert(*pos, *c);
    }
    for (_pos, c) in &all_pos {
        reduced_graph.add_node(Tile::try_from(*c).unwrap(), ());
    }
    for (pos, c) in &all_pos {
        // Reset
        for (_, node_ref) in &full_graph.nodes {
            let mut node = node_ref.borrow_mut();
            node.marked = false;
            node.distance = None;
        }
        full_graph.breath_first_search(*pos, FullBranchState::new());
        for (other_pos, other_c) in &all_pos {
            if other_pos == pos {
                continue;
            }
            let other = full_graph.nodes.get(other_pos).unwrap().borrow();
            match other.distance {
                Some(d) => {
                    reduced_graph.add_edge(Tile::try_from(*c).unwrap(), Tile::try_from(*other_c).unwrap(), d);
                }
                None => {}
            }
        }
    }
    reduced_graph
}

fn key_set_to_id(key_set: &HashSet<char>) -> u32 {
    key_set.iter().map(|c| 2u32.pow(((*c as u8) - 97).into())).sum()
}

fn id_to_key_set(id: u32) -> HashSet<char> {
    let mut key_set = HashSet::new();
    for (i, b) in format!("{:032b}", id).chars().rev().enumerate() {
        if b == '1' {
            key_set.insert((i + 97) as u8 as char);
        }
    }
    key_set
}

fn add_to_key_set_id(id: u32, key: char) -> u32 {
    assert!(key.is_ascii_lowercase());
    let n = 2u32.pow(((key as u8) - 97).into());
    if (id / n) % 2 == 0 {
        id + n
    } else {
        id  // Already in the set
    }
}

fn key_set_id_contains(id: u32, key: char) -> bool {
    assert!(key.is_ascii_lowercase());
    let n = 2u32.pow(((key as u8) - 97).into());
    if (id / n) % 2 == 0 {
        false
    } else {
        true
    }
}

fn key_set_has_key_for_door(id: u32, door: char) -> bool {
    assert!(door.is_ascii_uppercase());
    key_set_id_contains(id, door.to_ascii_lowercase())
}

// For problem 1
fn dijkstra1(graph: &ReducedGraph, source: &Tile) -> u32 {
    let mut processed: HashMap<u32, HashSet<Tile>> = HashMap::new();
    let mut distances: HashMap<u32, HashMap<Tile, u32>> = HashMap::new();
    let mut all_keys = HashSet::new();

    for (tile, _) in &graph.nodes {
        match tile {
            Tile::Key(c) => {all_keys.insert(*c);}
            _ => {}
        }
    }
    let all_keys_id = key_set_to_id(&all_keys);

    distances.insert(0, [(*source, 0)].iter().cloned().collect());

    while !processed.contains_key(&all_keys_id) {
        // Find smallest entry not in processed
        let mut min_dist_op: Option<u32> = None;
        let mut min_tile_op: Option<Tile> = None;
        let mut min_key_set_id_op: Option<u32> = None;
        for (key_set_id, key_set_dists) in &distances {
            for (tile, d) in key_set_dists {
                // Check if closed door
                match tile {
                    Tile::Door(c) => {
                        if !key_set_has_key_for_door(*key_set_id, *c) {
                            continue;
                        }
                    }
                    _ => {}
                }
                // Check if already processed
                match processed.get(key_set_id) {
                    Some(key_set_processed) => {
                        if key_set_processed.contains(tile) {
                            continue;
                        }
                    }
                    None => {}
                }
                if min_dist_op.is_none() || *d <= min_dist_op.unwrap() {
                    min_dist_op = Some(*d);
                    min_tile_op = Some(*tile);
                    min_key_set_id_op = Some(*key_set_id);
                }
            }
        }
        let min_dist = min_dist_op.unwrap();
        let min_tile = min_tile_op.unwrap();
        let min_key_set_id = min_key_set_id_op.unwrap();
        processed.entry(min_key_set_id).or_insert(HashSet::new()).insert(min_tile);

        for neigh in graph.neighbors.get(&min_tile).unwrap().borrow().iter() {
            let neigh_key_set = match neigh {
                Tile::Key(c) => {add_to_key_set_id(min_key_set_id, *c)}
                Tile::Start | Tile::Door(_) => {min_key_set_id}
                _ => {panic!("Other than key, door or start: {:?}", neigh);}
            };
            let length = graph.edge_data.get(&(min_tile, *neigh)).unwrap();
            let alt = min_dist + length;
            let key_set_dists = distances.entry(neigh_key_set).or_insert(HashMap::new());
            match key_set_dists.get(neigh) {
                Some(d) => {
                    if alt < *d {key_set_dists.insert(*neigh, alt);}
                }
                None => {key_set_dists.insert(*neigh, alt);}
            }
        }
    }
    *distances.get(&all_keys_id).unwrap().iter().min_by(|x, y| x.1.cmp(y.1)).unwrap().1
}

#[derive(Debug, Eq, PartialEq, Hash, Clone, Copy)]
struct State {
    tiles: [Tile; 4],
}

impl State {
    fn new(tiles: &Vec<Tile>) -> State {
        State{
            tiles: [tiles[0], tiles[1], tiles[2], tiles[3]],
        }
    }

    fn updated_state(&self, robot_id: usize, new_tile: Tile) -> State {
        let mut new_state = self.clone();
        new_state.tiles[robot_id] = new_tile;
        new_state
    }
}

impl fmt::Display for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut to_print = String::new();
        for i in 0..4 {
            let c: char = TryInto::<char>::try_into(self.tiles[i]).unwrap();
            to_print.push(c);
        }
        write!(f, "{}", to_print)
    }
}

// For problem 2
fn dijkstra2(graph: &ReducedGraph, sources: &Vec<Tile>) -> u32 {
    let mut processed: HashMap<u32, HashSet<State>> = HashMap::new();
    let mut distances: HashMap<u32, HashMap<State, u32>> = HashMap::new();
    let mut next_steps: HashMap<u32, HashSet<(u32, State)>> = HashMap::new();
    let mut all_keys = HashSet::new();

    for (tile, _) in &graph.nodes {
        match tile {
            Tile::Key(c) => {all_keys.insert(*c);}
            _ => {}
        }
    }
    let all_keys_id = key_set_to_id(&all_keys);

    distances.insert(0, [(State::new(sources), 0u32)].iter().cloned().collect());
    next_steps.insert(0, [(0, State::new(sources))].iter().cloned().collect());

    let mut collected_keys = 0;
    while !processed.contains_key(&all_keys_id) {
        let mut min_dist: u32;
        let min_state: State;
        let min_key_set_id: u32;
        loop {
            min_dist = *next_steps.keys().min().unwrap();
            let dist_set = next_steps.get_mut(&min_dist).unwrap();
            if dist_set.len() == 0 {
                continue;
            }
            let entry = *dist_set.iter().next().unwrap();
            dist_set.remove(&entry);
            min_key_set_id = entry.0;
            min_state = entry.1;
            break;
        }
        let num_keys = id_to_key_set(min_key_set_id).len();
        if num_keys > collected_keys {
            println!("Collected {} keys", num_keys);
            collected_keys = num_keys;
        }
        match next_steps.get_mut(&min_dist) {
            Some(dist_states) => {
                dist_states.remove(&(min_key_set_id, min_state));
            }
            None => {panic!("Should exist");}
        }
        if next_steps.get(&min_dist).or(Some(&HashSet::new())).unwrap().len() == 0 {
            next_steps.remove(&min_dist);
        }

        processed.entry(min_key_set_id).or_insert(HashSet::new()).insert(min_state);

        for (robot_id, min_tile) in min_state.tiles.iter().enumerate() {
            for neigh in graph.neighbors.get(min_tile).unwrap().borrow().iter() {
                let neigh_key_set = match neigh {
                    Tile::Key(c) => {add_to_key_set_id(min_key_set_id, *c)}
                    Tile::SplitStart(_) | Tile::Door(_) => {min_key_set_id}
                    _ => {panic!("Other than key, door or start: {:?}", neigh);}
                };
                let length = graph.edge_data.get(&(*min_tile, *neigh)).unwrap();
                let alt = min_dist + length;
                let key_set_dists = distances.entry(neigh_key_set).or_insert(HashMap::new());
                let new_state = min_state.updated_state(robot_id, *neigh);
                // Add to next step if not processed if already processed
                let mut locked_door = false;
                for tile in &new_state.tiles {
                    match tile {
                        Tile::Door(c) => {
                            if !key_set_has_key_for_door(neigh_key_set, *c) {
                                locked_door = true;
                            }
                        }
                        _ => {}
                    }
                }
                let is_processed = match processed.get(&neigh_key_set) {
                    Some(key_set_processed) => {
                        key_set_processed.contains(&new_state)
                    }
                    None => {false}
                };
                let mut old_d: Option<u32> = None;
                let is_closer = match key_set_dists.get(&new_state) {
                    Some(d) => {
                        old_d = Some(*d);
                        alt < *d
                    }
                    None => {true}
                };
                if is_closer {
                    key_set_dists.insert(new_state, alt);
                    if !locked_door && !is_processed {
                        match old_d {
                            Some(d) => {
                                if next_steps.contains_key(&d) {
                                    next_steps.get_mut(&d).unwrap().remove(&(neigh_key_set, new_state));
                                }
                            }
                            None => {}
                        }
                        next_steps.entry(alt).or_insert(HashSet::new()).insert((neigh_key_set, new_state));
                    }
                }
            }
        }
    }
    *distances.get(&all_keys_id).unwrap().iter().min_by(|x, y| x.1.cmp(y.1)).unwrap().1
}

fn problem_1() {
    let file = "data/day18.txt";
    let reduced_graph = load_reduced_graph(file);
    let source = Tile::try_from('@').unwrap();
    let steps = dijkstra1(&reduced_graph, &source);
    println!("{}", steps);
}

fn problem_2() {
    let file = "data/day18split.txt";
    let reduced_graph = load_reduced_graph(file);
    let raw_sources = "1234";
    let sources: Vec<Tile> = raw_sources.chars().map(|c| c.try_into().unwrap()).collect();
    let steps = dijkstra2(&reduced_graph, &sources);
    println!("{}", steps);
}

pub fn run() {
    // problem_1();
    problem_2();
}
