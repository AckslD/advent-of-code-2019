use std::fs::File;
use std::io::prelude::*;
use std::fmt;
use advent2019::toolbox::read_input;

const HEIGHT: usize = 6;
const WIDTH: usize = 25;

#[derive(Debug)]
struct Image {
    layers: Vec<Layer>,
}

#[derive(Debug)]
struct Layer {
    pixels: Vec<Vec<Pixel>>,
}

impl fmt::Display for Layer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output = String::new();
        for row in 0..HEIGHT {
            for col in 0..WIDTH {
                if self.pixels[row][col] == 0 {
                    output.push(' ');
                } else if self.pixels[row][col] == 1{
                    output.push('#');
                }
                // output.push((self.pixels[row][col] + 48) as char);
            }
            output.push('\n');
        }
        write!(f, "{}", output)
    }
}

type Pixel = u8;

fn parse_input(filepath: String) -> Image {
    let raw_str = read_input(filepath).replace("\n", "");
    let digits: Vec<u32> = raw_str.chars().map(|d| d.to_digit(10).unwrap()).collect();
    let mut offset = 0;
    let mut layers: Vec<Layer> = Vec::new();
    while offset < digits.len() {
        let mut pixels: Vec<Vec<Pixel>> = Vec::new();
        for row in 0..HEIGHT {
            let mut cols: Vec<Pixel> = Vec::new();
            for col in 0..WIDTH {
                let i = row * WIDTH + col + offset;
                let pixel_value = digits[i] as Pixel;
                cols.push(pixel_value);
            }
            pixels.push(cols);
        }
        let layer = Layer{pixels};
        layers.push(layer);
        offset += WIDTH * HEIGHT;
    }

    Image{layers}
}

fn count_digits(layer: &Layer, digit: Pixel) -> u32 {
    let mut count = 0;
    for row in 0..HEIGHT {
        for col in 0..WIDTH {
            if layer.pixels[row][col] == digit {
                count += 1
            }
        }
    }

    count
}

fn find_min_zero_layer(image: Image) -> Option<Layer> {
    let mut min_zero: Option<u32> = Option::None;
    let mut min_layer: Option<Layer> = Option::None;
    for layer in image.layers {
        let count = count_digits(&layer, 0);
        if let Some(min) = min_zero {
            if count < min {
                min_zero = Some(count);
                min_layer = Some(layer);
            }
        } else {
            min_zero = Some(count);
            min_layer = Some(layer);
        }
    }

    min_layer
}

fn decode_image(image: Image) -> Layer {
    let mut pixels: Vec<Vec<Pixel>> = Vec::new();
    for _ in 0..HEIGHT {
        let mut cols: Vec<Pixel> = Vec::new();
        for _ in 0..WIDTH {
            cols.push(2);
        }
        pixels.push(cols);
    }
    for layer in image.layers {
        for row in 0..HEIGHT {
            for col in 0..WIDTH {
                if pixels[row][col] == 2 {
                    pixels[row][col] = layer.pixels[row][col];
                }
            }
        }
    }

    Layer{pixels}
}

pub fn run() {
    let image = parse_input("data/day8.txt".to_string());

    // Problem 1
    // let min_layer = find_min_zero_layer(image).unwrap();
    // let num_ones = count_digits(&min_layer, 1);
    // let num_twos = count_digits(&min_layer, 2);
    // println!("{}", num_ones * num_twos);

    // Problem 2
    let decoded = decode_image(image);
    print!("{}", decoded);
}
