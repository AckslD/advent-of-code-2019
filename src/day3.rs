use std::collections::HashMap;


#[derive(Debug)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, PartialEq, Copy, Clone)]
enum Angle {
    Vertical,
    Horizontal,
}

#[derive(Debug)]
struct Movement {
    direction: Direction,
    steps: i64,
}

#[derive(Debug, PartialEq, Copy, Clone, Hash, Eq)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn manhattan_distance(self) -> i64 {
        self.x.abs() + self.y.abs()
    }

    fn manhattan_distance_to_other(self, other: Point) -> i64 {
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}


#[derive(Debug, Copy, Clone)]
struct Segment {
    start: Point,
    end: Point,
    angle: Angle,
    flipped: bool,
}

fn max(x: i64, y: i64) -> i64 {
    if x >= y {
        return x;
    } else {
        return y;
    }
}

fn min(x: i64, y: i64) -> i64 {
    if x <= y {
        return x;
    } else {
        return y;
    }
}


impl Segment {
    fn new(p1: Point, p2: Point) -> Segment {
        if p1.x == p2.x {
            if p1.y <= p2.y {
                return Segment{start: p1, end: p2, angle: Angle::Vertical, flipped: false}
            } else {
                return Segment{start: p2, end: p1, angle: Angle::Vertical, flipped: true}
            }
        } else {
            if p1.y != p2.y {
                panic!("Segment is neither vertical nor horizontal");
            }
            if p1.x <= p2.x {
                return Segment{start: p1, end: p2, angle: Angle::Horizontal, flipped: false}
            } else {
                return Segment{start: p2, end: p1, angle: Angle::Horizontal, flipped: true}
            }
        }
    }

    fn length(self) -> i64 {
        match self.angle {
            Angle::Horizontal => {(self.end.x - self.start.x).abs()}
            Angle::Vertical => {(self.end.y - self.start.y).abs()}
        }
    }

    fn has_point(self, p: Point) -> bool {
        match self.angle {
            Angle::Horizontal => {
                self.start.y == p.y && self.start.x <= p.x && p.x <= self.end.x
            }
            Angle::Vertical => {
                self.start.x == p.x && self.start.y <= p.y && p.y <= self.end.y
            }
        }
    }

    fn intersection(self, other: Segment) -> Vec<Point> {
        let mut points = Vec::new();
        match self.angle {
            Angle::Horizontal => {
                match other.angle {
                    Angle::Horizontal => {
                        if self.start.y != other.start.y {
                            return points;
                        }
                        for i in max(self.start.x, other.start.x)..(min(self.end.x, other.end.x)+1) {
                            points.push(Point{x: i, y: self.start.y});
                        }
                        return points;
                    }
                    Angle::Vertical => {
                        if self.start.y < other.start.y || self.start.y > other.end.y {
                            return points;
                        }
                        if other.start.x < self.start.x || other.start.x > self.end.x {
                            return points;
                        }
                        points.push(Point{x: other.start.x, y: self.start.y});
                        return points;
                    }
                }
            }
            Angle::Vertical => {
                match other.angle {
                    Angle::Horizontal => {
                        if other.start.y < self.start.y || other.start.y > self.end.y {
                            return points;
                        }
                        if self.start.x < other.start.x || self.start.x > other.end.x {
                            return points;
                        }
                        points.push(Point{x: self.start.x, y: other.start.y});
                        return points;
                    }
                    Angle::Vertical => {
                        if self.start.x != other.start.x {
                            return points;
                        }
                        for i in max(self.start.y, other.start.y)..(min(self.end.y, other.end.y)+1) {
                            points.push(Point{x: self.start.x, y: i});
                        }
                        return points;
                    }
                }
            }
        }
    }
}

fn get_lines() -> Vec<String> {
    let raw_strs = vec!(
        // "R8,U5,L5,D3",
        // "U7,R6,D4,L4",
        // "R75,D30,R83,U83,L12,D49,R71,U7,L72",
        // "U62,R66,U55,R34,D71,R55,D58,R83",
        // "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51",
        // "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7",
        "R990,U796,R784,U604,R6,U437,L96,U285,L361,U285,L339,D512,L389,D840,L425,U444,L485,D528,L262,U178,L80,U2,R952,U459,L361,D985,R56,U135,R953,D913,L361,U120,L329,U965,L294,U890,L126,U214,R232,D444,L714,U791,R888,U923,R378,U233,L654,D703,R902,D715,R469,D60,R990,U238,R755,U413,L409,D601,R452,U504,R472,D874,L766,D594,R696,U398,R593,D889,R609,D405,L962,U176,L237,U642,L393,D91,L463,U936,R199,D136,R601,D8,R359,D863,L410,U598,L444,D34,R664,D323,R72,D98,L565,D476,L197,D132,R510,U665,R936,U3,R385,U144,L284,D713,L605,U106,R543,D112,R528,D117,R762,U330,R722,U459,L229,U375,L870,D81,R623,U95,L148,D530,L622,D62,R644,D365,L214,U847,R31,D832,L648,D293,R79,D748,L270,U159,L8,U83,R195,U912,L409,D649,L750,D286,L623,D956,R81,U775,R44,D437,L199,U698,L42,U419,L883,U636,L323,U89,L246,D269,L992,U739,R62,U47,R63,U17,L234,U135,R126,D208,L69,U550,L123,D66,R463,U992,R411,D276,L851,U520,R805,D300,L894,U171,L922,D901,R637,U907,R328,U433,L316,D644,L398,U10,L648,D190,R884,U474,R397,D718,L925,D578,R249,U959,L697,D836,R231,U806,R982,U827,R579,U830,L135,D666,R818,D502,L898,D585,R91,D190,L255,U535,R56,U390,R619,D815,L300,D81,R432,D70,L940,D587,L259,D196,R241,U4,R440,U678,R185,U451,R733,D984,R464,D298,L738,U600,R353,D44,L458,U559,L726,D786,L307,D333,L226,D463,R138,D142,L521,D201,R51,D202,L204,U130,L333,U597,R298,U42,L951,U66,R312,U707,L555,D225,L360,D12,L956,D361,L989,D625,L944,D398,L171,D982,L377,U114,L339,U164,R39,D793,R992,U834,R675,U958,R334,D697,L734,D40,L149,U394,R976",
        "L1005,D52,L125,U787,L761,U262,L466,D966,R895,U789,R6,U2,R870,U971,R238,D946,L752,D240,R721,U349,L679,D518,L104,U417,L462,U544,L519,U797,R873,U70,R298,U45,L779,D921,R468,D421,R803,U108,L812,D498,R226,D309,R766,U724,L961,U472,R940,U944,R418,D682,R328,U55,R737,U961,L343,U397,R112,D292,L155,U162,R398,U445,L524,U256,R323,D587,L862,D726,R624,D230,R460,U539,R723,U93,L507,U608,L150,U159,R35,U458,R208,U546,L495,D835,L636,U960,L322,U408,L78,D250,L994,U818,R107,U978,R401,D147,R574,D549,R983,U698,L99,D63,L772,U409,R975,U990,L893,U467,L860,D721,R504,U102,R678,D672,L406,D933,R743,D788,R142,D44,R208,D424,R28,D674,R331,D968,L154,U206,R222,D354,R687,D331,L539,D390,L373,D514,L622,U673,R345,U943,L508,D337,R265,D785,L189,U429,R344,D719,R622,U199,L765,U350,R833,U309,R95,U911,R548,U746,R107,D867,L648,D680,R28,U596,L891,U168,R933,U571,R365,U267,R916,D130,L149,D898,L513,D167,R587,U799,R134,D328,R562,D929,L399,U568,R565,U241,L395,U822,L624,D145,L995,U516,R474,D609,R153,U52,R561,D15,R283,U321,L850,U218,L225,D635,L630,U102,L84,D672,L128,D885,L506,U911,R355,D132,R155,D120,L110,U368,R149,D343,L708,U378,R591,D585,L381,D517,R852,U432,R342,U273,R893,D277,L548,U859,L891,U311,L901,U255,R421,U90,L72,D474,L654,U12,L146,D867,L485,D663,R123,D82,L21,U408,L38,D864,L114,D645,R936,U765,L832,D668,L482,U79,L594,U276,L559,D469,R314,D162,R621,U230,L688,U82,R605,U191,L381,D327,L91,D217,R714,D942,R851,U358,R22,U952,R279,D897,R485,D867,L940,U891,L223,D264,L634,D560,R645,D705,L289,U584,R97,U920,R41",
    );
    let mut line_strs = Vec::new();
    for raw_str in raw_strs{
        line_strs.push(raw_str.to_string());
    }

    line_strs
}

fn string_to_line(line_str: String) -> Vec<Segment> {
    let mut movements = Vec::new();
    for elem in line_str.split_terminator(",") {
        let dir: Direction;
        match &elem[0..1] {
            "R" => {dir = Direction::Right;}
            "L" => {dir = Direction::Left;}
            "U" => {dir = Direction::Up;}
            "D" => {dir = Direction::Down;}
            _   => {panic!("unknown direction")}
        }
        let steps = i64::from_str_radix(&elem[1..], 10).unwrap();
        movements.push(Movement{direction: dir, steps});
    }

    let mut segments = Vec::new();
    let mut point = Point{x: 0, y: 0};
    let mut new_point = Point{x: 0, y: 0};
    for movement in movements {
        match movement.direction {
            Direction::Right => {new_point.x += movement.steps;}
            Direction::Left => {new_point.x -= movement.steps;}
            Direction::Up => {new_point.y += movement.steps;}
            Direction::Down => {new_point.y -= movement.steps;}
        }

        segments.push(Segment::new(point, new_point));

        point.x = new_point.x;
        point.y = new_point.y;
    }

    segments
}

fn get_intersections(line1: &Vec<Segment>, line2: &Vec<Segment>) -> Vec<Point> {
    let mut intersections = Vec::new();
    for segment1 in line1 {
        for segment2 in line2 {
            for point in segment1.intersection(*segment2) {
                intersections.push(point);
            }
        }
    }

    intersections
}

fn min_manhattan_distance(intersections: Vec<Point>) -> i64 {
    let mut min_dist = 0;
    for p in intersections {
        let dist = p.manhattan_distance();
        if min_dist == 0 {
            min_dist = dist;
        }
        if dist != 0 && dist < min_dist {
            min_dist = dist;
        }
    }

    min_dist
}

fn find_distance(line: &Vec<Segment>, end_point: Point) -> i64 {
    let mut distances = HashMap::new();
    let mut tot_distance: i64 = 0;
    for seg in line {
        // println!("seqment {:?}", seg);
        let distance = seg.length();
        tot_distance += distance;
        let point: Point;
        if seg.flipped {
            point = seg.start;
        } else {
            point = seg.end;
        }
        // println!("distance {}, tot_distance {}, and point {:?}", distance, tot_distance, point);
        tot_distance = *distances.entry(point).or_insert(tot_distance);
        // println!("distance {}, tot_distance {}, and point {:?}\n", distance, tot_distance, point);

        if seg.has_point(end_point) {
            // println!("ret {} - {}", tot_distance, point.manhattan_distance_to_other(end_point));
            return tot_distance - point.manhattan_distance_to_other(end_point)
        }
    }

    0
}

fn closest_intersection(line1: &Vec<Segment>, line2: &Vec<Segment>, intersections: Vec<Point>) -> i64 {
    let mut shortest_dist = 0;
    for p in intersections {
        let dist1 = find_distance(&line1, p);
        let dist2 = find_distance(&line2, p);
        // println!("dist1 {} and dist2 {}", dist1, dist2);
        let tot_dist = dist1 + dist2;

        if shortest_dist == 0 {
            shortest_dist = tot_dist;
        }
        if tot_dist != 0 && tot_dist < shortest_dist {
            shortest_dist = tot_dist;
        }
    }

    shortest_dist
}

pub fn run() {
    let line_strs = get_lines();
    let mut lines = Vec::new();
    for line_str in line_strs {
        let line = string_to_line(line_str);
        lines.push(line);
        // println!("{:?}", line);
    }
    let line1 = &lines[0];
    let line2 = &lines[1];

    let intersections = get_intersections(line1, line2);

    // Problem 1
    // let min_dist = min_manhattan_distance(intersections);

    // Problem 2
    let min_dist = closest_intersection(line1, line2, intersections);

    println!("{}", min_dist);
}
