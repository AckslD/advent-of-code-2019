use std::fmt;
use std::collections::{HashMap, VecDeque};
use advent2019::intcode::{Memory, Program, ProgramState, execute_program};
use advent2019::toolbox::read_input;

enum Direction {
    Up,
    Down,
    Left,
    Right,
}

struct Position {
    x: i32,
    y: i32,
}

#[derive(Debug, Clone, Copy)]
enum Color {
    Black,
    White,
}

struct Robot {
    pos: Position,
    dir: Direction,
}

impl Robot {
    fn new() -> Robot {
        Robot{pos: Position{x: 0, y: 0}, dir: Direction::Up}
    }

    fn forward(&mut self) {
        match self.dir {
            Direction::Up => self.pos.y += 1,
            Direction::Down => self.pos.y -= 1,
            Direction::Right => self.pos.x += 1,
            Direction::Left => self.pos.x -= 1,
        }
    }

    fn turn(&mut self, value: u8) {
        match value {
            0 => {
                match self.dir {
                    Direction::Up => self.dir = Direction::Left,
                    Direction::Down => self.dir = Direction::Right,
                    Direction::Right => self.dir = Direction::Up,
                    Direction::Left => self.dir = Direction::Down,
                }
            }
            1 => {
                match self.dir {
                    Direction::Up => self.dir = Direction::Right,
                    Direction::Down => self.dir = Direction::Left,
                    Direction::Right => self.dir = Direction::Down,
                    Direction::Left => self.dir = Direction::Up,
                }
            }
            _ => {panic!("Unknown direction value {}", value);}
        }
    }
}

struct Board {
    board: HashMap<(i32,i32),Color>,
}

impl Board {
    pub fn new() -> Board {
        Board{board: HashMap::new()}
    }

    pub fn read(&mut self, x: i32, y:i32) -> Color {
        *self.board.entry((x, y)).or_insert(Color::Black)
    }

    pub fn write(&mut self, x: i32, y:i32, value: Color) {
        self.board.insert((x, y), value);
    }

    pub fn range(&self) -> (i32, i32, i32, i32) {
        if self.board.len() == 0 {
            return (0, 0, 0, 0);
        }
        let mut xmin: Option<i32> = None;
        let mut xmax: Option<i32> = None;
        let mut ymin: Option<i32> = None;
        let mut ymax: Option<i32> = None;
        for ((x, y), _) in self.board.iter() {
            match xmin {
                Some(xm) => {
                    if *x < xm {
                        xmin = Some(*x);
                    }
                }
                None => {xmin = Some(*x)}
            }
            match xmax {
                Some(xm) => {
                    if *x > xm {
                        xmax = Some(*x);
                    }
                }
                None => {xmax = Some(*x)}
            }
            match ymin {
                Some(ym) => {
                    if *y < ym {
                        ymin = Some(*y);
                    }
                }
                None => {ymin = Some(*y)}
            }
            match ymax {
                Some(ym) => {
                    if *y > ym {
                        ymax = Some(*y);
                    }
                }
                None => {ymax = Some(*y)}
            }
        }

        (xmin.unwrap(), xmax.unwrap(), ymin.unwrap(), ymax.unwrap())
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output = String::new();
        let (xmin, xmax, ymin, ymax) = self.range();
        for y in (-ymax)..(-ymin + 1) {
            for x in xmin..(xmax + 1) {
                match self.board.get(&(x, -y)) {
                    Some(c) => {
                        match c {
                            Color::Black => output.push(' '),
                            Color::White => output.push('#'),
                        };
                    }
                    None => {output.push(' ')}
                }
            }
            output.push('\n');
        }
        write!(f, "{}", output)
    }
}

fn run_robot(board: &mut Board, robot: &mut Robot, program: &mut Program) {
    while program.state != ProgramState::Finished {
        let input: u8;
        match board.read(robot.pos.x, robot.pos.y) {
            Color::Black => input = 0,
            Color::White => input = 1,
        }
        program.input.push_back(input as i64);

        execute_program(program, ProgramState::Output);
        if program.state == ProgramState::Finished {
            break;
        }
        let color_value = program.output.pop().unwrap();
        let color: Color;
        match color_value {
            0 => color = Color::Black,
            1 => color = Color::White,
            _ => panic!("Unknown color value {}", color_value),
        }
        board.write(robot.pos.x, robot.pos.y, color);

        execute_program(program, ProgramState::Output);
        if program.state == ProgramState::Finished {
            break;
        }
        match program.output.pop() {
            Some(turn_value) => {
                robot.turn(turn_value as u8);
                robot.forward();
            }
            None => {
                println!("no more output");
                break;
            }
        }
    }
}

fn get_initial_program() -> Program {
    let raw_str = read_input("data/day11.txt".to_string());
    let raw_str = raw_str.replace("\n", "");
    let raw_program = raw_str.split_terminator(',').map(|d| d.parse::<i64>().unwrap()).collect();
    let input: VecDeque<i64> = VecDeque::new();
    let output: Vec<i64> = Vec::new();
    Program{
        memory: Memory{data: raw_program},
        input: input,
        output,
        instr_pointer: 0,
        state: ProgramState::Running,
        rel_base: 0,
    }
}

pub fn run() {
    // Program 1
    let mut program = get_initial_program();
    let mut board = Board::new();
    let mut robot = Robot::new();

    // run_robot(&mut board, &mut robot, &mut program);
    // println!("{}", board.board.len());

    // Problem 2
    board.write(0, 0, Color::White);
    run_robot(&mut board, &mut robot, &mut program);
    println!("{}", board);
}
