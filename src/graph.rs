use std::cell::RefCell;
use std::collections::{HashMap, HashSet, VecDeque};
use std::marker::PhantomData;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum BFSStatus {
    Process,
    Discard,
    Finished,
}

pub trait Node<T, E, S> {
    fn get_new_state(&mut self, from_id: &T, from_node: &Self, state: &S, edge_data: &E) -> S;
}

pub trait State<T, N> {
    fn get_status(&self) -> BFSStatus;
    fn get_steps(&self) -> u32;
}

pub struct DiGraph<T, N, E, D, S>
    {
    pub nodes: HashMap<T, RefCell<N>>,
    pub neighbors: HashMap<T, RefCell<HashSet<T>>>,
    pub edge_data: HashMap<(T, T), E>,
    _phantom: (PhantomData<D>, PhantomData<S>),
}

impl<T, N, E, D, S> DiGraph<T, N, E, D, S>
    where T: Clone + Copy + Eq + std::hash::Hash + std::fmt::Debug,
          E: Clone + Copy + Eq + std::hash::Hash + std::fmt::Debug,
          N: From<D> + std::fmt::Display,
    {
    pub fn new() -> DiGraph<T, N, E, D, S> {
        DiGraph::<T, N, E, D, S>{
            nodes: HashMap::new(),
            neighbors: HashMap::new(),
            edge_data: HashMap::new(),
            _phantom: (PhantomData, PhantomData),
        }
    }

    pub fn add_node(&mut self, name: T, data: D) {
        let node = RefCell::<N>::new(data.into());
        self.nodes.entry(name).or_insert(node);
    }
    
    // TODO
    pub fn remove_node(&mut self, name: &T) {
        self.nodes.remove(name);
        let neighbors = self.neighbors.get(name).unwrap().borrow();
        for neighbor in neighbors.iter() {
            let mut n = self.neighbors.get(neighbor).unwrap().borrow_mut();
            n.remove(name);
            self.edge_data.remove(&(*name, *neighbor));
            self.edge_data.remove(&(*neighbor, *name));
        }
    }

    pub fn add_edge(&mut self, start: T, end: T, data: E) {
        if !self.nodes.contains_key(&start) {
            panic!("Does not contain node {:?}", start);
        }
        if !self.nodes.contains_key(&end) {
            panic!("Does not contain node {:?}", end);
        }

        self.neighbors.entry(start).or_insert(RefCell::new(HashSet::new())).borrow_mut().insert(end);
        self.edge_data.insert((start, end), data);

        // TODO adding undirected edge
        self.neighbors.entry(end).or_insert(RefCell::new(HashSet::new())).borrow_mut().insert(start);
        self.edge_data.insert((end, start), data);
    }

    pub fn degree(&self, name: &T) -> Option<usize> {
        match self.neighbors.get(name) {
            Some(neigh) => Some(neigh.borrow().len()),
            None => None,
        }
    }

    pub fn has_edge(&self, start: &T, end: &T) -> bool {
        match self.neighbors.get(start) {
            Some(neighs) => {
                match neighs.borrow().get(end) {
                    Some(_) => true,
                    None => false,
                }
            }
            None => false,
        }
    }

    pub fn edge_data(&self, start: &T, end: &T) -> Option<E> {
        self.edge_data.get(&(*start, *end)).cloned()
    }
}

impl<T, N, E, D, S> DiGraph<T, N, E, D, S>
    where T: Clone + Copy + Eq + std::hash::Hash + std::fmt::Debug,
          E: Clone + Copy + Eq + std::hash::Hash + std::fmt::Debug,
          N: Node<T, E, S> + From<D> + std::fmt::Display,
          S: State<T, N>,
    {
    pub fn breath_first_search(&mut self, start_id: T, start_state: S) -> Option<u32> {
        let mut queue = VecDeque::new();
        queue.push_back((start_id, start_state));
        while queue.len() > 0 {
            let (node_id, node_state) = queue.pop_front().unwrap();
            let node = self.nodes.get(&node_id).unwrap().borrow();
            match self.neighbors.get(&node_id) {
                Some(neigh_ids) => {
                    for neigh_id in neigh_ids.borrow().iter() {
                        if *neigh_id == node_id {
                            continue;
                        }
                        let mut neigh_node = self.nodes.get(neigh_id).unwrap().borrow_mut();
                        let edge_data = self.edge_data.get(&(node_id, *neigh_id)).unwrap();
                        let new_state = neigh_node.get_new_state(&node_id, &node, &node_state, edge_data);
                        match new_state.get_status() {
                            BFSStatus::Process => {queue.push_back((*neigh_id, new_state));}
                            BFSStatus::Finished => {return Some(new_state.get_steps());}
                            BFSStatus::Discard => {}
                        }
                    }
                }
                _ => {}
            }
        }
        None
    }
}
