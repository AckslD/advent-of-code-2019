use std::fmt;
use std::collections::HashSet;
use advent2019::toolbox;
use advent2019::graph;
use advent2019::grid;

fn get_input() -> String {
    let raw_str = toolbox::read_input("data/day20.txt".to_string());
    raw_str
}

#[derive(Clone, Copy, PartialEq, Debug)]
enum TileType {
    Wall,
    Floor,
    Letter(char),
    Empty,
}

impl TileType {
    fn from(c: char) -> TileType {
        if c.is_ascii_uppercase() {
            TileType::Letter(c)
        } else if c == '.' {
            TileType::Floor
        } else if c == '#' {
            TileType::Wall
        } else if c == ' ' {
            TileType::Empty
        } else {
            panic!("Unkown tile {}", c);
        }
    }

    fn is_letter(&self) -> bool {
        match self {
            TileType::Letter(_) => true,
            _ => false,
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Portal {
    ids: (char, char),
    inside: bool,
}

impl Portal {
    fn new(ids: (char, char), inside: bool) -> Portal{
        Portal{
            ids,
            inside,
        }
    }
}

impl PartialEq for Portal {
    fn eq(&self, other: &Self) -> bool {
        self.ids == other.ids || self.ids == (other.ids.1, other.ids.0)
    }
}

struct Node {
    tile: char,
    tile_type: TileType,
    portal: Option<Portal>,
    marked: HashSet<u32>,
}

impl Node {
    fn new(tile: char) -> Node {
        Node{
            tile,
            tile_type: TileType::from(tile),
            portal: None,
            marked: HashSet::new(),
        }
    }
}

impl From<NodeData> for Node {
    fn from(data: NodeData) -> Node {
        Node::new(data)
    }
}

impl graph::Node<grid::Position, BranchState> for Node {
    fn get_new_state(&mut self, from_id: &grid::Position, from_node: &Self, state: &BranchState) -> BranchState {
        // println!("Processing node {} using state {:?}", self, state);
        let steps = state.steps + 1;
        let mut inside_depth = state.inside_depth;
        let mut allowed = true;
        match self.portal {
            Some(portal) => {
                if inside_depth == 0 && portal == Portal::new(('Z', 'Z'), false) {
                    return BranchState{status: graph::BFSStatus::Finished, steps, inside_depth: 0};
                }
                // Are we going though a portal?
                match &from_node.portal {
                    Some(from_portal) => {
                        // Just to make sure they're equal
                        assert_eq!(portal, *from_portal);
                        if !from_portal.inside {
                            assert!(portal.inside);
                            // Are we allowed to go out?
                            if inside_depth == 0 {
                                 allowed = false;
                            } else {
                                inside_depth -= 1;
                            }
                        } else {
                            assert!(!portal.inside);
                            inside_depth += 1;
                        }
                    }
                    None => {}
                }
            }
            None => {}
        }
        let status = if self.marked.contains(&inside_depth) || !allowed {
            graph::BFSStatus::Discard
        } else {
            self.marked.insert(inside_depth);
            graph::BFSStatus::Process
        };
        BranchState{status, steps, inside_depth}
    }
}

impl grid::TileNode for Node {
    fn get_tile(&self) -> char {
        self.tile
    }
}

impl fmt::Display for Node {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Node({})", self.tile)
    }
}

// struct NodeData {
//     tile: char,
// }

type NodeData = char;

#[derive(Debug)]
struct BranchState {
    status: graph::BFSStatus,
    steps: u32,
    inside_depth: u32,
}

impl graph::State<grid::Position, Node> for BranchState {
    fn get_status(&self) -> graph::BFSStatus {
        self.status
    }
    fn get_steps(&self) -> u32 {
        self.steps
    }
}

type Graph = grid::GridGraph<Node, NodeData, BranchState>;

fn load_full_graph() -> Graph {
    let raw_str = get_input();
    let mut row = 0;
    let mut col = 0;
    let mut empty_graph = Graph::new();
    let mut num_cols: usize = 0;
    let num_rows: usize;
    for c in raw_str.chars() {
        if c == '\n' {
            num_cols = col;
            col = 0;
            row += 1;
        } else {
            match TileType::from(c) {
                TileType::Wall | TileType::Empty => {}
                TileType::Floor | TileType::Letter(_) => {
                    let pos = grid::Position{x: col, y: row};
                    empty_graph.add_node(pos, c);
                }
            }
            col += 1
        }
    }
    num_rows = row;
    println!("size: {}, {}", num_rows, num_cols);

    let mut graph = Graph::new(); // NOTE not so nice to create another one but simple for now
    for (pos, node_ref) in &empty_graph.nodes {
        for neigh_pos_res in pos.surrounding() {
            if let Ok(neigh_pos) = neigh_pos_res {
                if let Some(neigh_ref) = empty_graph.nodes.get(&neigh_pos) {
                    let node = node_ref.borrow();
                    let neigh = neigh_ref.borrow_mut();
                    // Find neighbor of node to get full portal name
                    let mut portal: Option<(char, char)> = None;
                    if node.tile_type.is_letter() && neigh.tile_type == TileType::Floor {
                        for other_neigh_pos_res in pos.surrounding() {
                            if let Ok(other_neigh_pos) = other_neigh_pos_res {
                                if other_neigh_pos == neigh_pos {
                                    continue;
                                }
                                if let Some(other_neigh_ref) = empty_graph.nodes.get(&other_neigh_pos) {
                                    let other_neigh = other_neigh_ref.borrow();
                                    if other_neigh.tile_type.is_letter() {
                                        portal = Some((other_neigh.tile, node.tile));
                                        break;
                                    }
                                }
                            }
                        }
                        match portal {
                            None => {panic!("Could not produce full portal name");}
                            _ => {}
                        }
                    }
                    graph.add_node(*pos, node.tile);
                    graph.add_node(neigh_pos, neigh.tile);
                    graph.add_edge(*pos, neigh_pos);
                    if let Some(neigh_ref) = graph.nodes.get(&neigh_pos) {
                        let mut neigh = neigh_ref.borrow_mut();
                        // Find out if it's inside or not
                        let mut inside = true;
                        if pos.y == 1 || pos.y == num_rows-2 {
                            inside = false;
                        }
                        if pos.x == 1 || pos.x == num_cols-2 {
                            inside = false;
                        }
                        // Give the portal name to the closest floor node unless it already have
                        // one
                        match portal {
                            Some(p) => {
                                match neigh.portal {
                                    Some(_) => {}
                                    None => {neigh.portal = Some(Portal::new(p, inside));}
                                }
                            }
                            None => {}
                        }
                    }
                }
            }
        }
    }
    // Remove the portal nodes
    let mut portal_nodes: Vec<grid::Position> = Vec::new();
    for (pos, node_ref) in &graph.nodes {
        // let node = node_ref.borrow();
        if node_ref.borrow().tile_type.is_letter() {
            portal_nodes.push(*pos)
        }
    }
    for pos in portal_nodes {
        graph.remove_node(&pos);
    }

    // Connected nodes with portals
    let mut new_edges: Vec<(grid::Position, grid::Position)> = Vec::new();
    for (pos, node_ref) in &graph.nodes {
        match node_ref.borrow().portal {
            Some(portal) => {
                for (other_pos, other_node_ref) in &graph.nodes {
                    if pos == other_pos {
                        continue;
                    }
                    match other_node_ref.borrow().portal {
                        Some(other_portal) => {
                            if portal == other_portal {
                                new_edges.push((*pos, *other_pos));
                            }
                        }
                        None => {}
                    }
                }
            }
            None => {}
        }
    }
    for (pos, other_pos) in new_edges {
        graph.add_edge(pos, other_pos);
    }

    graph
}

fn find_start_pos(graph: &Graph) -> grid::Position {
    for (pos, node_ref) in &graph.nodes {
        match node_ref.borrow().portal {
            Some(portal) => {
                if portal == Portal::new(('A', 'A'), false) {
                    return *pos;
                }
            }
            None => {}
        }
    }
    panic!("Could not find start position");
}

pub fn run() {
    let mut graph = load_full_graph();
    // for (pos, node_ref) in &graph.nodes {
    //     let node = node_ref.borrow();
    //     println!("{} at {} at portal {:?} with neighbors {:?}", node, pos, node.portal, graph.neighbors.get(&pos));
    // }
    println!("{}", graph);
    let start = find_start_pos(&graph);
    let steps = graph.breath_first_search(
        start,
        BranchState{status: graph::BFSStatus::Process, steps: 0, inside_depth: 0},
    );
    println!("{}", steps);
}
