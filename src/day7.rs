use std::collections::VecDeque;
use itertools::Itertools;
use advent2019::intcode::{
    Program,
    ProgramState,
    Memory,
    execute_program
};

fn get_initial_program(input: VecDeque<i64>) -> Program {
    let raw_program: Vec<i64>;
    // raw_program = vec!(3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0);
    // raw_program = vec!(3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0);
    // raw_program = vec!(3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0);
    // raw_program = vec!(3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5);
    // raw_program = vec!(3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10);
    raw_program = vec!(3,8,1001,8,10,8,105,1,0,0,21,42,67,88,105,114,195,276,357,438,99999,3,9,101,4,9,9,102,3,9,9,1001,9,2,9,102,4,9,9,4,9,99,3,9,1001,9,4,9,102,4,9,9,101,2,9,9,1002,9,5,9,1001,9,2,9,4,9,99,3,9,1001,9,4,9,1002,9,4,9,101,2,9,9,1002,9,2,9,4,9,99,3,9,101,4,9,9,102,3,9,9,1001,9,5,9,4,9,99,3,9,102,5,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,99);
    let output: Vec<i64> = Vec::new();
    Program{
        memory: Memory{data: raw_program},
        input: input,
        output,
        instr_pointer: 0,
        state: ProgramState::Running,
        rel_base: 0,
    }
}

fn max_thruster_signal(version: u8) -> i64 {
    let num_amp: i64 = 5;
    let mut max_signal = -1;
    let mut setting: Vec<i64>;
    let permutations;
    match version {
        1 => {permutations = (0..num_amp).permutations(num_amp as usize)}
        2 => {permutations = (num_amp..(2*num_amp)).permutations(num_amp as usize)}
        _ => {panic!("Unknown version {}", version);}
    }
    for perm in permutations {
        // setting = vec!(9,8,7,6,5);
        setting = perm;

        let signal = check_setting(&setting, num_amp, version);
        if signal > max_signal {
            max_signal = signal;
        }
    }

    max_signal
}

fn check_setting(setting: &Vec<i64>, num_amp: i64, version: u8) -> i64 {
    let mut output = 0;
    match version {
        1 => {
            for i in 0..num_amp {
                let mut input = VecDeque::new();
                input.push_back(setting[i as usize]);
                input.push_back(output);
                let mut program = get_initial_program(input);
                execute_program(&mut program, ProgramState::Finished);
                output = program.output[program.output.len() - 1];
            }

            output
        }
        2 => {
            // Initialize the programs
            let mut programs: Vec<Program> = Vec::new();
            for i in 0..num_amp {
                let mut input = VecDeque::new();
                input.push_back(setting[i as usize]);
                if i == 0 {
                    input.push_back(0);
                }
                programs.push(get_initial_program(input));
            }
            
            loop {
                for i in 0..num_amp {
                // for program in programs {
                    let program = &mut programs[i as usize];
                    // println!("{}: input: {:?}", i, program.input);
                    execute_program(program, ProgramState::Output);
                    // println!("{}: output: {:?}", i, program.output);
                    // println!("{}: state: {:?}\n", i, program.state);
                    if i == (num_amp - 1) && program.state == ProgramState::Finished {
                        return program.output[program.output.len() - 1];
                    }
                    output = program.output[program.output.len() - 1];
                    // output = program.output.pop().expect("No more output");
                    // Reset the state
                    program.state = ProgramState::Running;
                    let next_i = (i + 1) % num_amp;
                    programs[next_i as usize].input.push_back(output);
                }
            }
        }
        _ => {panic!("Unknown version {}", version);}
    }
}

pub fn run() {
    // Problem 1 and 2
	let max_th = max_thruster_signal(2);
	println!("{}", max_th);
}
