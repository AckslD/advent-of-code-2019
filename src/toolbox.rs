use std::fs::File;
use std::io::prelude::*;

pub fn read_input(filepath: String) -> String {
    let mut file = File::open(filepath).unwrap();
    let mut raw_str = String::new();
    file.read_to_string(&mut raw_str).unwrap();
    raw_str
}

pub fn get_digit(number: i64, i: i64, base: i64) -> i64 {
    let n = i64::pow(base, i as u32);
    (number / n) % base
}

pub fn get_digits(number: i64, base: i64) -> Vec<i64> {
    let mut i = 0;
    let mut rev_digits = Vec::new();
    while i64::pow(base, i as u32) <= number {
        rev_digits.push(get_digit(number, i, base));
        i += 1;
    }

    rev_digits.reverse();
    rev_digits
}
