use advent2019::intcode::{Program, ProgramState, execute_program, Memory};
use advent2019::grid::Position;

fn get_initial_program() -> Program {
    let raw_program: Vec<i64> = vec!(109,424,203,1,21102,1,11,0,1106,0,282,21101,18,0,0,1105,1,259,1201,1,0,221,203,1,21102,1,31,0,1105,1,282,21102,38,1,0,1106,0,259,20102,1,23,2,21201,1,0,3,21101,1,0,1,21101,0,57,0,1106,0,303,1202,1,1,222,20101,0,221,3,21001,221,0,2,21101,259,0,1,21102,80,1,0,1106,0,225,21102,1,145,2,21101,91,0,0,1105,1,303,2101,0,1,223,20101,0,222,4,21102,1,259,3,21101,0,225,2,21101,0,225,1,21102,1,118,0,1105,1,225,21001,222,0,3,21101,80,0,2,21101,133,0,0,1105,1,303,21202,1,-1,1,22001,223,1,1,21101,148,0,0,1106,0,259,1201,1,0,223,20102,1,221,4,20101,0,222,3,21101,0,23,2,1001,132,-2,224,1002,224,2,224,1001,224,3,224,1002,132,-1,132,1,224,132,224,21001,224,1,1,21102,1,195,0,106,0,109,20207,1,223,2,21001,23,0,1,21102,1,-1,3,21102,1,214,0,1105,1,303,22101,1,1,1,204,1,99,0,0,0,0,109,5,2101,0,-4,249,22101,0,-3,1,22102,1,-2,2,21201,-1,0,3,21101,0,250,0,1105,1,225,21202,1,1,-4,109,-5,2105,1,0,109,3,22107,0,-2,-1,21202,-1,2,-1,21201,-1,-1,-1,22202,-1,-2,-2,109,-3,2106,0,0,109,3,21207,-2,0,-1,1206,-1,294,104,0,99,22101,0,-2,-2,109,-3,2105,1,0,109,5,22207,-3,-4,-1,1206,-1,346,22201,-4,-3,-4,21202,-3,-1,-1,22201,-4,-1,2,21202,2,-1,-1,22201,-4,-1,1,21202,-2,1,3,21102,1,343,0,1106,0,303,1105,1,415,22207,-2,-3,-1,1206,-1,387,22201,-3,-2,-3,21202,-2,-1,-1,22201,-3,-1,3,21202,3,-1,-1,22201,-3,-1,2,22101,0,-4,1,21102,384,1,0,1105,1,303,1106,0,415,21202,-4,-1,-4,22201,-4,-3,-4,22202,-3,-2,-2,22202,-2,-4,-4,22202,-3,-2,-3,21202,-4,-1,-2,22201,-3,-2,1,21202,1,1,-4,109,-5,2106,0,0);
    Program::new(raw_program)
}

fn prepare_program(pos: Position) -> Program {
    let mut program = get_initial_program();
    program.input.push_back(pos.x as i64);
    program.input.push_back(pos.y as i64);
    program
}

// struct Position {
//     x: u32,
//     y: u32,
// }

fn get_beam_status(pos: Position) -> bool {
    let mut program = prepare_program(pos);
    execute_program(&mut program, ProgramState::Finished);
    match program.output[0] {
        0 => false,
        1 => true,
        _ => panic!("Unknown output")
    }
}

fn paint_beam(xmin: usize, xmax: usize, ymin: usize, ymax: usize) {
    for y in ymin..ymax {
        for x in xmin..xmax {
            let pos = Position{x, y};
            let symbol = match get_beam_status(pos) {
            // match get_beam_status(pos) {
                true => '#',
                // true => {println!("{}, {}", x, y);}
                false => '.',
                // false => {}
            };
            print!("{}", symbol);
            if x == xmax-1 {
                print!("\n");
            }
        }
    }
}

fn count_affected_pos(xmax: usize, ymax: usize) -> usize {
    let mut sum = 0;
    for y in 0..ymax {
        for x in 0..xmax {
            let pos = Position{x, y};
            match get_beam_status(pos) {
                true => {sum += 1},
                false => {},
            }
        }
    }
    sum
}

pub fn problem_1() {
    let xmax = 50;
    let ymax = 50;
    paint_beam(0, xmax, 0, ymax);
    let sum = count_affected_pos(xmax, ymax);
    println!("sum = {}", sum);
}

fn fits_square(pos: Position, size: usize) -> bool {
    for y in 0..size {
        for x in 0..size {
            let p = Position{x: x+pos.x, y: y+pos.y};
            match get_beam_status(p) {
                true => {},
                false => {return false;},
            }
        }
    }
    true
}

struct Beam {
    pos: Position,
    last_xmin: Option<usize>,
    last_xmax: Option<usize>,
}

impl Beam {
    fn new(start: Position) -> Beam {
        Beam{
            pos: Position{x: start.x, y: start.y},
            last_xmin: None,
            last_xmax: None,
        }
    }
}

impl Iterator for Beam {
    type Item = Position;
    fn next(&mut self) -> Option<Self::Item> {
        match self.last_xmin {
            Some(_) => {}
            None => {
                match self.pos.left() {
                    Ok(pos_left) => match get_beam_status(pos_left) {
                        true => {
                            self.pos = pos_left;
                            // println!("Got positive beam for left {}", pos_left);
                            return Some(pos_left);
                        }
                        false => {self.last_xmin=Some(self.pos.x);}
                    }
                    Err(_) => {self.last_xmin=Some(self.pos.x);}
                }
            }
        }
        match self.last_xmax {
            Some(_) => {}
            None => {
                match self.pos.right() {
                    Ok(pos_right) => match get_beam_status(pos_right) {
                        true => {
                            // println!("Got positive beam for right {}", pos_right);
                            self.pos = pos_right;
                            return Some(pos_right);
                        }
                        false => {self.last_xmax=Some(self.pos.x);}
                    }
                    Err(_) => {self.last_xmax=Some(self.pos.x);}
                }
            }
        }
        self.pos = Position{
            x: (self.last_xmin.unwrap() + self.last_xmax.unwrap()) / 2,
            y: self.pos.y+1,
        };
        if !get_beam_status(self.pos) {
            panic!("Outside beam for new line");
        }
        self.last_xmin = None;
        self.last_xmax = None;
        // println!("Went to new line at {}", self.pos);
        Some(self.pos)
    }
}

fn paint_beam_from_iterator(start: Position, size: usize) {
    let beam = Beam::new(start);
    let mut char_grid: Vec<Vec<char>> = vec![vec![' '; size]; size];
    for pos in beam {
        // println!("pos = {}", pos);
        let diff_x = pos.x - start.x;
        let diff_y = pos.y - start.y;
        if diff_x >= size && diff_y >= size {
            break;
        }
        if diff_x >= size {
            continue;
        }
        if diff_y >= size {
            continue;
        }
        char_grid[pos.y - start.y][pos.x - start.x] = '#';
        // positions.push(pos);
    }
    for char_row in char_grid {
        println!("{}", char_row.into_iter().collect::<String>());
    }
    // println!("{:?}", char_grid);
}

fn find_fits_square(start: Position , size: usize) -> Position {
    let beam = Beam::new(start);
    for pos in beam {
        if fits_square(pos, size) {
            return pos;
        }
    }
    panic!("Couldn't fit square");
}

fn find_xmax(from: Position, lim_min: usize, lim_max: usize) -> usize {
    match get_beam_status(from) {
        true => {find_xmax_from_in(from, lim_max)}
        false => {find_xmax_from_out(from, lim_min)}
    }
}

fn find_xmax_from_out(from: Position, until: usize) -> usize {
    let mut pos = from;
    while !get_beam_status(pos) {
        pos = pos.left().unwrap();
        if pos.x < until {
            panic!("Could not find xmax");
        }
    }
    pos.x
}

fn find_xmax_from_in(from: Position, until: usize) -> usize {
    let mut pos = from;
    while get_beam_status(pos) {
        pos = pos.right().unwrap();
        if pos.x > until {
            panic!("Could not find xmax");
        }
    }
    pos.left().unwrap().x
}

fn find_xmin(from: Position, lim_min: usize, lim_max: usize) -> usize {
    match get_beam_status(from) {
        true => {find_xmin_from_in(from, lim_min)}
        false => {find_xmin_from_out(from, lim_max)}
    }
}

fn find_xmin_from_out(from: Position, until: usize) -> usize {
    let mut pos = from;
    while !get_beam_status(pos) {
        pos = pos.right().unwrap();
        if pos.x > until {
            panic!("Could not find xmin");
        }
    }
    pos.x
}

fn find_xmin_from_in(from: Position, until: usize) -> usize {
    let mut pos = from;
    while get_beam_status(pos) {
        pos = pos.left().unwrap();
        if pos.x < until {
            panic!("Could not find xmin");
        }
    }
    pos.left().unwrap().x
}

fn find_limits_at_line(line: usize) -> (usize, usize) {
    let xmin = find_xmin(Position{x: 0, y: line}, 0, line);
    let xmax = find_xmax(Position{x: line, y: line}, 0, line);
    (xmin, xmax)
}

fn fits_square_at_line(size: usize, line: usize) -> bool {
    let (_xmin, xmax) = find_limits_at_line(line);
    if xmax < size {
        return false;
    }
    let up_left = Position{x: xmax-size+1, y: line};
    // println!("up_left = {}", up_left);
    if !get_beam_status(up_left) {
        return false;
    }
    let low_left = Position{x: up_left.x, y: up_left.y+size-1};
    // println!("low_left = {}", low_left);
    if !get_beam_status(low_left) {
        return false;
    }
    // println!("here");
    true
}

fn find_wide_enough(size: usize) -> usize {
    let mut line = 10;
    loop {
        let (xmin, xmax) = find_limits_at_line(line);
        if xmax - xmin >= size {
            break;
        }
        line *= 2;
    }
    loop {
        let (xmin, xmax) = find_limits_at_line(line);
        if xmax - xmin < size {
            break;
        }
        line -= 1;
    }
    line + 1
}

fn find_fit_line(size: usize) -> usize {
    let mut line = 100;
    // loop {
    //     if fits_square_at_line(size, line) {
    //         break;
    //     }
    //     line *= 2;
    // }
    loop {
        if fits_square_at_line(size, line) {
            break;
        }
        line += 1;
    }
    line
}

fn find_pos_fit(size: usize) -> Position {
    let line = find_fit_line(size);
    let (_xmin, xmax) = find_limits_at_line(line);
    Position{x: xmax-size+1, y: line}
}

pub fn run() {
    // problem_1();
    // let line = 500;
    // println!("{:?}", find_limits_at_line(1045));
    // println!("{}", fits_square_at_line(10, 110));
    // println!("{}", get_beam_status(Position{x:80, y:120}));
    // paint_beam(698, 870, 1044, 1150);
    // paint_beam(764, 870, 1045, 1150);
    // let start = Position{x: 14, y: 20};
    // paint_beam_from_iterator(start, 50);
    // let pos = find_fits_square(start, 7);
    // let beam = Beam::new(start);
    // for pos in beam {
    //     println!("{}", pos);
    // }
    // let xmin = find_xmin(Position{x: 0, y: line});
    // let xmax = find_xmax(Position{x: line, y: line});
    // println!("{} - {} on line {}", xmin, xmax, line);
    let pos = find_pos_fit(100);
    println!("{}: {}", pos, pos.x * 10000 + pos.y);
    // println!("{}", find_pos_fit(100));
    // (763, 1045)
    // 7631045
    // paint_beam(xmin, xmax+10, line, line+50);
}
