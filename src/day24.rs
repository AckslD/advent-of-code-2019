use std::fmt;
use std::convert::TryFrom;
use std::collections::{HashSet, HashMap};
use advent2019::toolbox::read_input;

const SIZE: usize = 5;

#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Position {
    row: usize,
    col: usize,
    depth: i32,
}

impl Position {
    fn new(row: usize, col: usize, depth: i32) -> Position {
        assert!(!(row == 2 && col == 2));
        assert!(row < SIZE);
        assert!(col < SIZE);
        Position{row, col, depth}
    }

    fn surrounding(&self) -> Vec<Position> {
        let mut positions = Vec::new();
        // Above
        if self.row == 0 {
            positions.push(Position{row: 1, col: 2, depth: self.depth-1});
        } else if self.row == 3 && self.col == 2 {
            for col in 0..SIZE {
                positions.push(Position{row: SIZE-1, col, depth: self.depth+1});
            }
        } else {
            positions.push(Position{row: self.row-1, col: self.col, depth: self.depth});
        }
        // Below
        if self.row == SIZE-1 {
            positions.push(Position{row: 3, col: 2, depth: self.depth-1});
        } else if self.row == 1 && self.col == 2 {
            for col in 0..SIZE {
                positions.push(Position{row: 0, col, depth: self.depth+1});
            }
        } else {
            positions.push(Position{row: self.row+1, col: self.col, depth: self.depth});
        }
        // Left
        if self.col == 0 {
            positions.push(Position{row: 2, col: 1, depth: self.depth-1});
        } else if self.row == 2 && self.col == 3 {
            for row in 0..SIZE {
                positions.push(Position{row, col: SIZE-1, depth: self.depth+1});
            }
        } else {
            positions.push(Position{row: self.row, col: self.col-1, depth: self.depth});
        }
        // Right
        if self.col == SIZE-1 {
            positions.push(Position{row: 2, col: 3, depth: self.depth-1});
        } else if self.row == 2 && self.col == 1 {
            for row in 0..SIZE {
                positions.push(Position{row, col: 0, depth: self.depth+1});
            }
        } else {
            positions.push(Position{row: self.row, col: self.col+1, depth: self.depth});
        }
        positions
    }
}

// struct ErisIter<'a> {
//     row: usize,
//     col: usize,
//     eris: &'a Eris,
// }

// impl<'a> ErisIter<'a> {
//     fn new(eris: &Eris) -> ErisIter {
//         ErisIter{
//             row: 0,
//             col: 0,
//             eris,
//         }
//     }
// }

// impl<'a> Iterator for ErisIter<'a> {
//     type Item = (bool, Position);

//     fn next(&mut self) -> Option<Self::Item> {
//         if self.row == SIZE {
//             return None;
//         }
//         let pos = Position::new(self.row, self.col);
//         let alive = self.eris.is_alive(&pos);
//         self.col += 1;
//         if self.col == SIZE {
//             self.row += 1;
//             self.col = 0;
//         }
//         Some((alive, pos))
//     }
// }

struct Eris {
    tiles: HashSet<Position>,
}


impl Eris {
    fn update(&mut self) {
        let mut flips: HashSet<Position> = HashSet::new();
        for alive_pos in &self.tiles {
            if self.should_flip(alive_pos) {
                flips.insert(*alive_pos);
            }
            for neigh_pos in alive_pos.surrounding() {
                if self.should_flip(&neigh_pos) {
                    flips.insert(neigh_pos);
                }
            }
        }
        for pos in flips {
            self.flip(&pos);
        }
    }

    fn should_flip(&self, position: &Position) -> bool {
        let alive_count = position.surrounding().iter().filter(|p| self.is_alive(p)).count();
        if self.is_alive(position) {
            if alive_count != 1 {
                return true;
            }
        } else {
            if alive_count == 1 || alive_count == 2 {
                return true;
            }
        }
        false
    }

    fn flip(&mut self, position: &Position) {
        if self.tiles.contains(position) {
            self.tiles.remove(position);
        } else {
            self.tiles.insert(*position);
        }
    }

    fn is_alive(&self, position: &Position) -> bool {
        self.tiles.contains(position)
    }

    // fn iter(&self) -> ErisIter {
    //     ErisIter::new(self)
    // }

    // fn biodiversity_rating(&self) -> u32 {
    //     let mut sum = 0;
    //     for (i, (alive, _)) in self.iter().enumerate() {
    //         if alive {
    //             sum += 2u32.pow(i as u32);
    //         }
    //     }
    //     sum
    // }

    fn paint_grid(&self, depth: i32) {
        for row in 0..SIZE {
            for col in 0..SIZE {
                if row == 2 && col == 2 {
                    print!("?");
                    continue;
                }
                let pos = Position::new(row, col, depth);
                if self.is_alive(&pos) {
                    print!("#");
                } else {
                    print!(".");
                }
            }
            print!("\n");
        }
    }
}

impl TryFrom<&String> for Eris {
    type Error = &'static str;

    fn try_from(raw_str: &String) -> Result<Self, Self::Error> {
        let mut row = 0;
        let mut col = 0;
        let mut tiles: HashSet<Position> = HashSet::new();
        for c in raw_str.chars() {
            match c {
                '\n' => {
                    row += 1;
                    col = 0;
                }
                '#' => {
                    let position = Position::new(row, col, 0);
                    tiles.insert(position);
                    col += 1;
                }
                '.' => {col += 1;}
                _ => {return Err("Unkown tile");}
            }
        }
        Ok(Eris{tiles})
    }
}

// impl fmt::Display for Eris {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         let mut to_print = String::new();
//         for row in 0..SIZE {
//             for col in 0..SIZE {
//                 if self.tiles[row][col] {
//                     to_print.push('#');
//                 } else {
//                     to_print.push('.');
//                 }
//             }
//             to_print.push('\n');
//         }
//         write!(f, "{}", to_print)
//     }
// }

pub fn run() {
    let raw_str = read_input("data/day24.txt".to_string());
    let mut eris = Eris::try_from(&raw_str).unwrap();
    println!("{:?}", eris.tiles);
    for _ in 0..200 {
        eris.update();
    }
    for depth in -5..6 {
        println!("Depth {}:", depth);
        eris.paint_grid(depth);
        println!("");
    }
    println!("{}", eris.tiles.len());
}
