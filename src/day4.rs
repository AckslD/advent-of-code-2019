fn is_valid(digits: Vec<u32>) -> bool {
    let mut double = false;
    for i in 0..(digits.len()-1) {
        if digits[i] > digits[i + 1] {
            return false;
        }
        if digits[i] == digits[i + 1] {
            double = true;
        }
    }

    double
}

fn is_valid2(digits: Vec<u32>) -> bool {
    let mut double = false;
    let mut current_seq_len = 1;
    for i in 0..(digits.len()-1) {
        if digits[i] > digits[i + 1] {
            return false;
        } else if digits[i] == digits[i + 1] {
            current_seq_len += 1;
        } else {
            if current_seq_len == 2 {
                double = true;
            }
            current_seq_len = 1;
        }
    }

    if current_seq_len == 2 {
        double = true;
    }
    // println!("digits {:?} : {}", digits, double);
    double
}

fn count_passwords(min: u32, max: u32, version: u32) -> u32 {
    let mut count = 0;
    for i in min..(max + 1) {
        let digits: Vec<u32> = i.to_string().chars().map(|d| d.to_digit(10).unwrap()).collect();
        match version {
            1 => {
                if is_valid(digits) {
                    count += 1;
                }
            }
            2 => {
                if is_valid2(digits) {
                    count += 1;
                }
            }
            _ => {panic!("Unknown version {}", version)}
        }
    }

    count
}



pub fn run() {
    // Problem 1
    // println!("{}", count_passwords(256310, 732736, 1));

    // Problem 2
    println!("{}", count_passwords(256310, 732736, 2));
    // println!("{}", count_passwords(330000, 340000, 2));
}
