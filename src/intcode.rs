use std::collections::VecDeque;
use crate::toolbox::get_digit;

#[derive(Debug)]
enum Instruction {
    Add,
    Mul,
    Input,
    Output,
    JumpTrue,
    JumpFalse,
    Less,
    Equal,
    RelBase,
    Exit,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Mode {
    Position,
    Immediate,
    Relative,
}

#[derive(Debug, PartialEq)]
pub enum ProgramState {
    Running,
    Output,
    Input,
    Waiting,
    Finished,
}

#[derive(Debug)]
pub struct Memory {
    pub data: Vec<i64>,
}

impl Memory {
    pub fn read(&mut self, index: i64) -> i64 {
        let i = index as usize;
        if i >= self.data.len() {
            for _ in 0..(i - self.data.len() + 1) {
                self.data.push(0);
            }
        }

        self.data[i]
    }

    pub fn write(&mut self, index: i64, value: i64) {
        let i = index as usize;
        if i >= self.data.len() {
            for _ in 0..(i - self.data.len() + 1) {
                self.data.push(0);
            }
        }

        self.data[i] = value;
    }
}

#[derive(Debug)]
pub struct Program {
    pub memory: Memory,
    pub input: VecDeque<i64>,
    pub output: VecDeque<i64>,
    pub instr_pointer: i64,
    pub state: ProgramState,
    pub rel_base: i64,
}

impl Program {
    pub fn new(raw_program: Vec<i64>) -> Program {
        let output: VecDeque<i64> = VecDeque::new();
        Program{
            memory: Memory{data: raw_program},
            input: VecDeque::new(),
            output,
            instr_pointer: 0,
            state: ProgramState::Running,
            rel_base: 0,
        }
    }
}

fn get_jump(instr: Instruction) -> i64 {
    match instr {
        Instruction::Add => 4,
        Instruction::Mul => 4,
        Instruction::Input => 2,
        Instruction::Output => 2,
        Instruction::JumpTrue => 0,
        Instruction::JumpFalse => 0,
        Instruction::Less => 4,
        Instruction::Equal => 4,
        Instruction::RelBase => 2,
        Instruction::Exit => 1,
    }
}

fn do_instruction(program: &mut Program) -> &Program {
    let i = program.instr_pointer;
    let (instr, modes) = parse_raw_instr(program.memory.read(i));
    // println!("Instr: {:?}", instr);
    // println!("Program before: {:#?}", program);
    match instr {
        Instruction::Add => {do_add(program, i, &modes);}
        Instruction::Mul => {do_mul(program, i, &modes);}
        Instruction::Input => {do_input(program, i, &modes);}
        Instruction::Output => {do_output(program, i, &modes);}
        Instruction::JumpTrue => {do_jump_true(program, i, &modes);}
        Instruction::JumpFalse => {do_jump_false(program, i, &modes);}
        Instruction::Less => {do_less(program, i, &modes);}
        Instruction::Equal => {do_equal(program, i, &modes);}
        Instruction::RelBase => {do_rel_base(program, i, &modes);}
        Instruction::Exit => {program.state = ProgramState::Finished;}
    }
    if program.state != ProgramState::Waiting {
        program.instr_pointer += get_jump(instr);
    }
    // println!("Program after: {:#?}", program);
    program
}

fn do_add(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op1 = program.memory.read(i + 1);
    let op2 = program.memory.read(i + 2);
    let op3 = program.memory.read(i + 3);
    let value = get_op_value(op1, program, modes[0]) + get_op_value(op2, program, modes[1]);
    let index = get_index(op3, program, modes[2]);
    program.memory.write(index, value);
}


fn do_mul(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op1 = program.memory.read(i + 1);
    let op2 = program.memory.read(i + 2);
    let op3 = program.memory.read(i + 3);
    let value = get_op_value(op1, program, modes[0]) * get_op_value(op2, program, modes[1]);
    let index = get_index(op3, program, modes[2]);
    program.memory.write(index, value);
}

fn do_input(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    // println!("doing input");
    let op = program.memory.read(i + 1);
    match program.input.pop_front() {
        Some(inp) => {
            let index = get_index(op, program, modes[0]);
            program.memory.write(index, inp);
            program.state = ProgramState::Input;
        }
        // None => {panic!("No more input");}
        None => {
            program.state = ProgramState::Waiting;
        }
    }
}

fn do_output(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op = program.memory.read(i + 1);
    let output = get_op_value(op, program, modes[0]);
    // println!("doing output {}", output);
    program.output.push_back(output);

    program.state = ProgramState::Output;
}

fn do_jump_true(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op1 = program.memory.read(i + 1);
    let op2 = program.memory.read(i + 2);
    if get_op_value(op1, program, modes[0]) == 0 {
        program.instr_pointer += 3;
    } else {
        program.instr_pointer = get_op_value(op2, program, modes[1]);
    }
}

fn do_jump_false(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op1 = program.memory.read(i + 1);
    let op2 = program.memory.read(i + 2);
    if get_op_value(op1, program, modes[0]) == 0 {
        program.instr_pointer = get_op_value(op2, program, modes[1]);
    } else {
        program.instr_pointer += 3;
    }
}

fn do_less(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op1 = program.memory.read(i + 1);
    let op2 = program.memory.read(i + 2);
    let op3 = program.memory.read(i + 3);
    let mut value = 0;
    if get_op_value(op1, program, modes[0]) < get_op_value(op2, program, modes[1]) {
        value = 1;
    }
    let index = get_index(op3, program, modes[2]);
    program.memory.write(index, value);
}

fn do_equal(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op1 = program.memory.read(i + 1);
    let op2 = program.memory.read(i + 2);
    let op3 = program.memory.read(i + 3);
    let mut value = 0;
    if get_op_value(op1, program, modes[0]) == get_op_value(op2, program, modes[1]) {
        value = 1;
    }
    let index = get_index(op3, program, modes[2]);
    program.memory.write(index, value);
}

fn do_rel_base(program: &mut Program, i: i64, modes: &Vec<Mode>) {
    let op = program.memory.read(i + 1);
    program.rel_base += get_op_value(op, program, modes[0]);
}

fn get_op_value(op: i64, program: &mut Program, mode: Mode) -> i64 {
    match mode {
        Mode::Position => program.memory.read(op),
        Mode::Immediate => op,
        Mode::Relative => program.memory.read(program.rel_base + op),
    }
}

fn get_index(op: i64, program: &mut Program, mode: Mode) -> i64 {
    match mode {
        Mode::Position => op,
        Mode::Relative => program.rel_base + op,
        Mode::Immediate => {panic!("Mode should not be immediate for writing!");},
    }
}

fn parse_raw_instr(raw_instr: i64) -> (Instruction, Vec<Mode>) {
    // Extract instruction
    let instr: Instruction;
    match raw_instr % 100 {
        1 => {instr = Instruction::Add;}
        2 => {instr = Instruction::Mul;}
        3 => {instr = Instruction::Input;}
        4 => {instr = Instruction::Output;}
        5 => {instr = Instruction::JumpTrue;}
        6 => {instr = Instruction::JumpFalse;}
        7 => {instr = Instruction::Less;}
        8 => {instr = Instruction::Equal;}
        9 => {instr = Instruction::RelBase;}
        99 => {instr = Instruction::Exit;}
        _ => {panic!("not a valid instruction {}", raw_instr);}
    }

    // Extract modes
    let mut modes = Vec::new();
    for i in 2..5 {
        let raw_mode = get_digit(raw_instr, i, 10);
        let mode: Mode;
        match raw_mode {
            0 => {mode = Mode::Position;}
            1 => {mode = Mode::Immediate;}
            2 => {mode = Mode::Relative;}
            _ => {panic!("Unknown mode {}", raw_mode);}
        }
        modes.push(mode);
    }
    (instr, modes)
}

pub fn execute_program(program: &mut Program, stop_on: ProgramState) {
    program.state = ProgramState::Running;
    while program.state != ProgramState::Finished && program.state != stop_on {
        do_instruction(program);
        if program.state == ProgramState::Waiting && stop_on != ProgramState::Waiting {
            panic!("No more inputs and not waiting for state Waiting");
        }
    }
}
