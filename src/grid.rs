use std::fmt;
use std::cmp::Ordering;
use std::ops::{Deref, DerefMut};
use crate::graph;

#[derive(PartialEq, Eq, Clone, Copy, std::hash::Hash, Debug)]
pub struct Position {
    pub x: usize,
    pub y: usize,
}

impl Position {
    pub fn up(&self) -> Result<Position, &'static str> {
        if self.y == 0 {
            Err("No up")
        } else {
            Ok(Position{x: self.x, y: self.y-1})
        }
    }
    pub fn down(&self) -> Result<Position, &'static str> {
        Ok(Position{x: self.x, y: self.y+1})
    }
    pub fn right(&self) -> Result<Position, &'static str> {
        Ok(Position{x: self.x+1, y: self.y})
    }
    pub fn left(&self) -> Result<Position, &'static str> {
        if self.x == 0 {
            Err("No left")
        } else {
            Ok(Position{x: self.x-1, y: self.y})
        }
    }
    pub fn surrounding(&self) -> Vec<Result<Position, &'static str>> {
        // TODO should be variable length instead
        vec!(
            self.up(),
            self.down(),
            self.right(),
            self.left(),
        )
    }
    pub fn is_next_to(&self, other: &Position) -> bool {
        for neigh_res in self.surrounding() {
            match neigh_res {
                Ok(neigh) => {
                    if neigh == *other {return true;}
                }
                Err(_) => {}
            }
        }
        false
    }
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl Ord for Position {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.x, self.y).cmp(&(other.x, other.y))
    }
}

impl PartialOrd for Position {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some((self.x, self.y).cmp(&(other.x, other.y)))
    }
}

pub struct GridGraph<N, E, D, S>(graph::DiGraph<Position, N, E, D, S>);
pub struct SimpleGridGraph<N, D, S>(GridGraph<N, (), D, S>);

impl<N, E, D, S> GridGraph<N, E, D, S>
    where N: graph::Node<Position, E, S> + From<D> + std::fmt::Display,
          E: Clone + Copy + Eq + std::hash::Hash + std::fmt::Debug,
          // E: std::fmt::Debug,
          S: graph::State<Position, N>,
    {
    pub fn new() -> GridGraph<N, E, D, S> {
        GridGraph{0: graph::DiGraph::new()}
    }
}

impl<N, E, D, S> Deref for GridGraph<N, E, D, S> {
    type Target = graph::DiGraph<Position, N, E, D, S>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<N, E, D, S> DerefMut for GridGraph<N, E, D, S> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

pub trait TileNode {
    fn get_tile(&self) -> char;
}

impl<N, E, D, S> fmt::Display for GridGraph<N, E, D, S> 
    where N: TileNode,
    {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut max_col = 0;
        let mut max_row = 0;
        for (pos, _) in &self.nodes {
            if pos.x > max_col {
                max_col = pos.x;
            }
            if pos.y > max_row {
                max_row = pos.y;
            }
        }
        let mut output = String::new();
        for row in 0..(max_row+1) {
            for col in 0..(max_col+1) {
                if let Some(node) = self.nodes.get(&Position{x: col, y: row}) {
                    output.push(node.borrow().get_tile());
                } else {
                    output.push(' ');
                }
            }
            output.push('\n');
        }

        write!(f, "{}", output)
    }
}
