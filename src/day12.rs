use std::cmp::PartialEq;
use advent2019::numbers::gcd;

const DIMENSION: usize = 3;

struct Vector3 {
    x: i32,
    y: i32,
    z: i32,
}

struct Moon {
    pos: [i32; DIMENSION],
    vel: [i32; DIMENSION],
}

impl Moon {
    fn new(x: i32, y: i32, z: i32) -> Moon {
        Moon{
            pos: [x, y, z],
            vel: [0, 0, 0],
        }
    }

    fn evolve(&mut self) {
        for i in 0..DIMENSION {
            self.pos[i] += self.vel[i];
        }
    }

    fn potential_energy(&self) -> u32 {
        self.pos.iter().map(|p| p.abs() as u32).sum()
    }

    fn kinectic_energy(&self) -> u32 {
        self.vel.iter().map(|p| p.abs() as u32).sum()
    }

    fn energy(&self) -> u32 {
        self.potential_energy() * self.kinectic_energy()
    }
}

impl PartialEq for Moon {
    fn eq(&self, other: &Self) -> bool {
        for d in 0..DIMENSION {
            if self.pos[d] != other.pos[d] {
                return false;
            }
            if self.vel[d] != other.vel[d] {
                return false;
            }
        }
        true
    }
}

fn apply_gravity(moons: &mut Vec<Moon>, i: usize, j: usize) {
    for d in 0..DIMENSION {
        if moons[i].pos[d] < moons[j].pos[d] {
            moons[i].vel[d] += 1;
            moons[j].vel[d] -= 1;
        } else if moons[i].pos[d] > moons[j].pos[d] {
            moons[i].vel[d] -= 1;
            moons[j].vel[d] += 1;
        }
    }
}

fn read_input() -> String {
    // let raw_str = "<x=-1, y=0, z=2>
// <x=2, y=-10, z=-7>
// <x=4, y=-8, z=8>
// <x=3, y=5, z=-1>";
    // let raw_str = "<x=-8, y=-10, z=0>
// <x=5, y=5, z=10>
// <x=2, y=-7, z=3>
// <x=9, y=-8, z=-3>";
    let raw_str = "<x=-17, y=9, z=-5>
<x=-1, y=7, z=13>
<x=-19, y=12, z=5>
<x=-6, y=-6, z=-4>";
    raw_str.to_string()
}

fn load_moons() -> Vec<Moon> {
    let mut raw_str = read_input();
    raw_str = raw_str.replace("<", "").replace(">", "");
    let mut moons: Vec<Moon> = Vec::new();
    for row in raw_str.split_terminator('\n') {
        let mut pos: [i32; DIMENSION] = [0; DIMENSION];
        let mut i = 0;
        for val in row.split_terminator(", ") {
            let val: Vec<&str> = val.split_terminator("=").collect();
            pos[i] = val[1].parse::<i32>().unwrap();
            i += 1;
        }
        moons.push(Moon::new(pos[0], pos[1], pos[2]));
    }

    moons
}

fn simulate(moons: &mut Vec<Moon>, timesteps: u32) {
    for _ in 0..timesteps {
        // Apply gravity
        for i in 0..moons.len() {
            for j in (i+1)..moons.len() {
                apply_gravity(moons, i, j);
            }
        }

        // update position
        for i in 0..moons.len() {
            moons[i].evolve();
        }
    }
}

fn equal_state(dimension: usize, initial_moons: &Vec<Moon>, moons: &Vec<Moon>) -> bool {
    for i in 0..initial_moons.len() {
        if initial_moons[i].pos[dimension] != moons[i].pos[dimension] {
            return false;
        }
        if initial_moons[i].vel[dimension] != moons[i].vel[dimension] {
            return false;
        }
    }
    true
}

fn find_time_steps() -> i64 {
    let initial_moons = load_moons();
    let mut timesteps: Vec<i64> = Vec::new();
    for d in 0..DIMENSION {
        let mut found = false;
        let mut moons = load_moons();
        let mut t = 1;
        loop {
            simulate(&mut moons, 1);
            if equal_state(d, &initial_moons, &moons) {
                timesteps.push(t);
                found = true;
                break;
            }
            t += 1;
        }
        if !found {
            timesteps.push(0);
        }
    }
    compute_minimal_multiple(&timesteps)
}

fn compute_minimal_multiple(numbers: &Vec<i64>) -> i64 {
    assert_eq!(numbers.len(), 3);
    let a = numbers[0];
    let b = numbers[1];
    let c = numbers[2];
    let d = a * b / gcd(a, b);
    d * c / gcd(d, c)
}

pub fn run() {
    // Problem 1
    // let mut moons = load_moons();
    // for moon in &moons {
    //     println!("{:?} : {:?}", moon.pos, moon.vel);
    // }
    // simulate(&mut moons, 2028);

    // println!("");
    // let mut total_energy = 0;
    // for moon in &moons {
    //     println!("{:?} : {:?} : {}", moon.pos, moon.vel, moon.energy());
    //     total_energy += moon.energy();
    // }
    // println!("{}", total_energy);

    // Problem 2
    let timesteps = find_time_steps();
    println!("{}", timesteps);
}
