use std::fmt;
use std::collections::HashMap;
extern crate num_integer;
extern crate num;

pub fn gcd(a: i64, b: i64) -> i64 {
    if b == 0 {
        a
    } else {
        gcd(b, a % b)
    }
}

pub fn total_gcd(numbers: &Vec<i64>) -> i64 {
    if numbers.len() == 0 {
        return 1;
    }
    if numbers.len() == 1 {
        return numbers[0];
    }
    let mut tmp_gcd = gcd(numbers[0], numbers[1]);
    for i in 2..numbers.len() {
        tmp_gcd = gcd(tmp_gcd, numbers[i]);
    }
    tmp_gcd
}

#[derive(Debug)]
pub struct Number {
    prime_factors: HashMap<u32, u32>,
    is_zero: bool
}

impl Number {
    pub fn new(n: u32) -> Number {
        let mut p_factors: HashMap<u32, u32> = HashMap::new();
        if n == 0 {
            Number{prime_factors: p_factors, is_zero: true}
        } else {
            for p in prime_factors(n) {
                let num_p_factors = *p_factors.entry(p).or_insert(0);
                p_factors.insert(p, num_p_factors + 1);
            }

            Number{prime_factors: p_factors, is_zero: false}
        }
    }

    pub fn mul(&mut self, other: Number) {
        if self.is_zero {
            return;
        }
        if other.is_zero {
            self.prime_factors.clear();
            self.is_zero = true;
            return;
        }
        for (p, n) in other.prime_factors.iter() {
            for _ in 0..*n {
                let num_p_factors = *self.prime_factors.entry(*p).or_insert(0);
                self.prime_factors.insert(*p, num_p_factors + 1);
            }
        }
    }

    fn div(&mut self, other: Number) {
        if self.is_zero {
            return;
        }
        if other.is_zero {
            panic!("Divide by zero");
        }
        for (p, n) in other.prime_factors.iter() {
            for _ in 0..*n {
                let num = *self.prime_factors.get(p).unwrap_or(&0);
                if num == 0 {
                    panic!("Cannot divide");
                }
                self.prime_factors.insert(*p, num - 1);
            }
        }
    }

    fn to_number(&self) -> u32 {
        if self.is_zero {
            return 0;
        }
        let mut prod = 1;
        for (p, num) in self.prime_factors.iter() {
            for _ in 0..(*num) {
                prod *= p;
            }
        }

        prod
    }

    fn to_number_mod(&self, m: u32) -> u32 {
        if self.is_zero {
            return 0;
        }
        let mut prod = 1;
        for (p, num) in self.prime_factors.iter() {
            for _ in 0..(*num) {
                prod *= p % m;
                prod %= m;
            }
        }

        prod % m
    }
}

impl fmt::Display for Number {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.to_number())
    }
}

fn prime_factors(x: u32) -> Vec<u32> {
    let mut p_factors: Vec<u32> = Vec::new();
    let primes = prime_numbers((x as f32).sqrt() as u32 + 1);
    let mut x = x;
    let mut found_factor;
    while x > 1 {
        found_factor = false;
        for p in &primes {
            while x % p == 0 {
                p_factors.push(*p);
                x /= p;
                found_factor = true;
            }
        }
        if !found_factor {
            p_factors.push(x);
            break;
        }
    }

    p_factors
}

fn prime_numbers(x: u32) -> Vec<u32> {
    let mut is_prime: HashMap<u32, bool> = HashMap::new();
    is_prime.insert(2, true);
    let mut i = 3;
    while i <= x {
        is_prime.insert(i, true);
        i += 2;
    }
    let mut n = 2;
    while n <= x {
        let mut i = 2;
        while i * n <= x {
            if (i * n) % 2 != 0 {
                is_prime.insert(i * n, false);
            }
            i += 1;
        }
        if n == 2 {
            n += 1;
        } else {
            n += 2;
        }
    }

    let mut primes: Vec<u32> = Vec::new();
    for (p, is_p) in is_prime.iter() {
        if *is_p {
            primes.push(*p);
        }
    }

    primes
}


fn hyper_triangular_number(k: u32, n: u32) -> u32 {
    if n <= k {
        return 0;
    }
    let mut num = Number::new(1);
    for i in 0..(k+1) {
        num.mul(Number::new(n - i));
    }
    num.div(factorial(k+1));

    num.to_number()
}

fn binomial(n: u32, k: u32) -> u32 {
    let mut f = factorial(n);
    f.div(factorial(k));
    f.div(factorial(n - k));
    f.to_number()
}

fn factorial(x: u32) -> Number {
    let mut n = Number::new(1);
    for i in 2..(x + 1) {
        n.mul(Number::new(i));
    }
    n
}
