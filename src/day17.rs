use std::fmt;
use advent2019::intcode::{Program, ProgramState, execute_program, Memory};
use advent2019::grid::Position;

fn get_initial_program() -> Program {
    let raw_program: Vec<i64> = vec!(1,330,331,332,109,3438,1102,1182,1,16,1101,1449,0,24,101,0,0,570,1006,570,36,101,0,571,0,1001,570,-1,570,1001,24,1,24,1105,1,18,1008,571,0,571,1001,16,1,16,1008,16,1449,570,1006,570,14,21101,58,0,0,1106,0,786,1006,332,62,99,21102,333,1,1,21102,73,1,0,1106,0,579,1101,0,0,572,1101,0,0,573,3,574,101,1,573,573,1007,574,65,570,1005,570,151,107,67,574,570,1005,570,151,1001,574,-64,574,1002,574,-1,574,1001,572,1,572,1007,572,11,570,1006,570,165,101,1182,572,127,101,0,574,0,3,574,101,1,573,573,1008,574,10,570,1005,570,189,1008,574,44,570,1006,570,158,1105,1,81,21101,0,340,1,1106,0,177,21101,0,477,1,1105,1,177,21101,0,514,1,21102,1,176,0,1105,1,579,99,21102,1,184,0,1106,0,579,4,574,104,10,99,1007,573,22,570,1006,570,165,101,0,572,1182,21101,0,375,1,21102,211,1,0,1106,0,579,21101,1182,11,1,21102,222,1,0,1106,0,979,21102,1,388,1,21102,233,1,0,1105,1,579,21101,1182,22,1,21101,244,0,0,1105,1,979,21102,401,1,1,21102,255,1,0,1105,1,579,21101,1182,33,1,21102,1,266,0,1105,1,979,21102,1,414,1,21101,277,0,0,1106,0,579,3,575,1008,575,89,570,1008,575,121,575,1,575,570,575,3,574,1008,574,10,570,1006,570,291,104,10,21101,1182,0,1,21102,1,313,0,1106,0,622,1005,575,327,1101,1,0,575,21102,327,1,0,1105,1,786,4,438,99,0,1,1,6,77,97,105,110,58,10,33,10,69,120,112,101,99,116,101,100,32,102,117,110,99,116,105,111,110,32,110,97,109,101,32,98,117,116,32,103,111,116,58,32,0,12,70,117,110,99,116,105,111,110,32,65,58,10,12,70,117,110,99,116,105,111,110,32,66,58,10,12,70,117,110,99,116,105,111,110,32,67,58,10,23,67,111,110,116,105,110,117,111,117,115,32,118,105,100,101,111,32,102,101,101,100,63,10,0,37,10,69,120,112,101,99,116,101,100,32,82,44,32,76,44,32,111,114,32,100,105,115,116,97,110,99,101,32,98,117,116,32,103,111,116,58,32,36,10,69,120,112,101,99,116,101,100,32,99,111,109,109,97,32,111,114,32,110,101,119,108,105,110,101,32,98,117,116,32,103,111,116,58,32,43,10,68,101,102,105,110,105,116,105,111,110,115,32,109,97,121,32,98,101,32,97,116,32,109,111,115,116,32,50,48,32,99,104,97,114,97,99,116,101,114,115,33,10,94,62,118,60,0,1,0,-1,-1,0,1,0,0,0,0,0,0,1,12,16,0,109,4,1202,-3,1,586,21001,0,0,-1,22101,1,-3,-3,21101,0,0,-2,2208,-2,-1,570,1005,570,617,2201,-3,-2,609,4,0,21201,-2,1,-2,1106,0,597,109,-4,2106,0,0,109,5,1201,-4,0,630,20102,1,0,-2,22101,1,-4,-4,21102,1,0,-3,2208,-3,-2,570,1005,570,781,2201,-4,-3,653,20101,0,0,-1,1208,-1,-4,570,1005,570,709,1208,-1,-5,570,1005,570,734,1207,-1,0,570,1005,570,759,1206,-1,774,1001,578,562,684,1,0,576,576,1001,578,566,692,1,0,577,577,21102,702,1,0,1106,0,786,21201,-1,-1,-1,1106,0,676,1001,578,1,578,1008,578,4,570,1006,570,724,1001,578,-4,578,21102,1,731,0,1105,1,786,1106,0,774,1001,578,-1,578,1008,578,-1,570,1006,570,749,1001,578,4,578,21102,756,1,0,1106,0,786,1106,0,774,21202,-1,-11,1,22101,1182,1,1,21102,1,774,0,1106,0,622,21201,-3,1,-3,1105,1,640,109,-5,2105,1,0,109,7,1005,575,802,20101,0,576,-6,20101,0,577,-5,1106,0,814,21101,0,0,-1,21102,0,1,-5,21102,0,1,-6,20208,-6,576,-2,208,-5,577,570,22002,570,-2,-2,21202,-5,51,-3,22201,-6,-3,-3,22101,1449,-3,-3,1202,-3,1,843,1005,0,863,21202,-2,42,-4,22101,46,-4,-4,1206,-2,924,21102,1,1,-1,1105,1,924,1205,-2,873,21101,0,35,-4,1106,0,924,1202,-3,1,878,1008,0,1,570,1006,570,916,1001,374,1,374,1201,-3,0,895,1102,2,1,0,2102,1,-3,902,1001,438,0,438,2202,-6,-5,570,1,570,374,570,1,570,438,438,1001,578,558,922,20102,1,0,-4,1006,575,959,204,-4,22101,1,-6,-6,1208,-6,51,570,1006,570,814,104,10,22101,1,-5,-5,1208,-5,39,570,1006,570,810,104,10,1206,-1,974,99,1206,-1,974,1101,1,0,575,21101,973,0,0,1106,0,786,99,109,-7,2106,0,0,109,6,21102,1,0,-4,21102,1,0,-3,203,-2,22101,1,-3,-3,21208,-2,82,-1,1205,-1,1030,21208,-2,76,-1,1205,-1,1037,21207,-2,48,-1,1205,-1,1124,22107,57,-2,-1,1205,-1,1124,21201,-2,-48,-2,1105,1,1041,21101,-4,0,-2,1106,0,1041,21102,-5,1,-2,21201,-4,1,-4,21207,-4,11,-1,1206,-1,1138,2201,-5,-4,1059,1202,-2,1,0,203,-2,22101,1,-3,-3,21207,-2,48,-1,1205,-1,1107,22107,57,-2,-1,1205,-1,1107,21201,-2,-48,-2,2201,-5,-4,1090,20102,10,0,-1,22201,-2,-1,-2,2201,-5,-4,1103,2101,0,-2,0,1106,0,1060,21208,-2,10,-1,1205,-1,1162,21208,-2,44,-1,1206,-1,1131,1106,0,989,21102,439,1,1,1105,1,1150,21101,0,477,1,1106,0,1150,21102,514,1,1,21102,1,1149,0,1105,1,579,99,21102,1,1157,0,1106,0,579,204,-2,104,10,99,21207,-3,22,-1,1206,-1,1138,1201,-5,0,1176,2102,1,-4,0,109,-6,2105,1,0,38,7,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,20,9,9,7,5,1,28,1,9,1,11,1,28,1,9,1,9,9,22,1,9,1,9,1,1,1,5,1,22,1,9,1,5,7,5,1,22,1,9,1,5,1,3,1,7,1,22,13,3,1,3,1,7,1,32,1,1,1,3,1,3,1,7,14,19,9,1,1,7,2,33,1,3,1,1,1,1,1,7,2,17,7,9,1,3,5,1,8,17,1,5,1,9,1,5,1,3,1,6,1,15,5,3,1,9,7,3,1,6,1,15,1,1,1,1,1,3,1,19,1,6,9,7,1,1,9,17,1,14,1,7,1,3,1,3,1,1,1,17,1,14,1,7,1,3,1,3,13,7,1,14,1,7,1,3,1,5,1,9,1,7,1,14,1,5,7,5,1,9,1,7,1,14,1,5,1,1,1,9,1,9,1,7,1,14,9,9,1,9,1,7,1,20,1,11,1,9,1,7,1,20,1,5,7,9,9,20,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,1,5,1,44,7,30);
    Program::new(raw_program)
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum Tile {
    Empty,
    Scaffold,
    RobotUp,
    RobotDown,
    RobotLeft,
    RobotRight,
    Crash,
    Intersection,
}

impl Tile {
    pub fn from_char(c: char) -> Result<Tile, String> {
        match c {
            '.' => Ok(Tile::Empty),
            '#' => Ok(Tile::Scaffold),
            '^' => Ok(Tile::RobotUp),
            'v' => Ok(Tile::RobotDown),
            '<' => Ok(Tile::RobotLeft),
            '>' => Ok(Tile::RobotRight),
            'X' => Ok(Tile::Crash),
            'O' => Ok(Tile::Intersection),
            _ => Err(format!("Unknown tile {}", c))
        }
    }

    pub fn to_char(&self,) -> char {
        match self {
            Tile::Empty => '.',
            Tile::Scaffold => '#',
            Tile::RobotUp => '^',
            Tile::RobotDown => 'v',
            Tile::RobotLeft => '<',
            Tile::RobotRight => '>',
            Tile::Crash => 'X',
            Tile::Intersection => 'O',
        }
    }
}

struct Board {
    board: Vec<Vec<Tile>>,
    num_rows: usize,
    num_cols: usize,
}

impl Board {
    pub fn new(raw_string: &String) -> Board {
        let mut board = Vec::<Vec::<Tile>>::new();
        let mut row = Vec::<Tile>::new();
        for c in raw_string.chars() {
            if c == '\n' {
                if row.len() > 0 {
                    board.push(row);
                    row = Vec::new();
                } else {
                    break;
                }
            } else {
                let tile = Tile::from_char(c).unwrap();
                row.push(tile);
            }
        }
        let num_rows = board.len();
        let num_cols = board[0].len();
        Board{board: board, num_rows: num_rows, num_cols: num_cols}
    }

    pub fn get_tile(&self, pos: &Position) -> Tile {
        self.board[pos.y][pos.x]
    }

    pub fn get_tile_result(&self, pos: Result<Position, &'static str>) -> Result<Tile, &'static str> {
        match pos {
            Ok(p) => {
                if p.y >= self.num_rows || p.x >= self.num_cols {
                    Err("No tile")
                } else {
                    if p.y >= self.board.len() {
                        panic!("{} >= {}", p.y, self.board.len());
                    }
                    if p.x >= self.board[p.y].len() {
                        panic!("{} >= {}", p.x, self.board[p.y].len());
                    }
                    Ok(self.board[p.y][p.x])
                }
            }
            Err(_) => Err("No tile"),
        }
    }

    pub fn set_tile(&mut self, pos: &Position, tile: Tile) {
        self.board[pos.y][pos.x] = tile;
    }
}

impl fmt::Display for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut output = String::new();
        for row in &self.board {
            for tile in row {
                output.push(tile.to_char());
            }
            output.push('\n');
        }
        write!(f, "{}", output)
    }
}

fn load_initial_board() -> Board {
    let mut program = get_initial_program();
    execute_program(&mut program, ProgramState::Finished);
    let mut raw_string = String::new();
    for p in &program.output {
        raw_string.push(*p as u8 as char);
    }
    Board::new(&raw_string)
}

fn run_movements(input: &String) -> Board {
    let mut program = get_initial_program();
    program.memory.data[0] = 2;
    for c in input.chars() {
        program.input.push_back(c as i64);
    }
    execute_program(&mut program, ProgramState::Finished);
    println!("{:?}", program.output);
    let mut raw_string = String::new();
    for p in &program.output {
        raw_string.push(*p as u8 as char);
    }
    println!("{}", raw_string);
    Board::new(&raw_string)
}

fn is_intersection_pos(board: &Board, pos: &Position) -> bool {
    if board.get_tile(pos) != Tile::Scaffold {
        return false;
    }
    for neigh in pos.surrounding() {
        if board.get_tile(pos) != Tile::Scaffold {
            return false;
        }
    }
    true
}

fn find_intersections(board: &Board) -> Vec<Position> {
    let mut intersections = Vec::new();
    for i in 1..(board.num_rows-1) {
        for j in 1..(board.num_cols-1) {
            let pos = Position{x: j, y: i};
            if is_intersection_pos(board, &pos) {
                intersections.push(pos);
            }
        }
    }
    intersections
}

fn get_aligment(pos: &Position) -> usize {
    pos.x * pos.y
}

#[derive(Debug)]
enum Movement {
    Right,
    Left,
    Forward,
    Backward,
}

fn find_robot(board: &Board) -> (Position, Tile) {
    for i in 1..(board.num_rows-1) {
        for j in 1..(board.num_cols-1) {
            let pos = Position{x: j, y: i};
            let tile = board.get_tile(&pos);
            match tile {
                Tile::RobotUp => return (pos, tile),
                Tile::RobotDown => return (pos, tile),
                Tile::RobotLeft => return (pos, tile),
                Tile::RobotRight => return (pos, tile),
                _ => continue
            }
        }
    }
    panic!("Robot not found");
}

fn next_movement(board: &Board, robot_pos: Position, robot_tile: Tile) -> (Movement, Position, Tile) {
    match robot_tile {
        Tile::RobotUp => {
            if board.get_tile_result(robot_pos.up()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Forward, robot_pos.up().unwrap(), Tile::RobotUp)
            } else if board.get_tile_result(robot_pos.right()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Right, robot_pos, Tile::RobotRight)
            } else if board.get_tile_result(robot_pos.left()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Left, robot_pos, Tile::RobotLeft)
            } else {
                panic!("No scaffolding to go to");
            }
        }
        Tile::RobotDown => {
            if board.get_tile_result(robot_pos.down()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Forward, robot_pos.down().unwrap(), Tile::RobotDown)
            } else if board.get_tile_result(robot_pos.left()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Right, robot_pos, Tile::RobotLeft)
            } else if board.get_tile_result(robot_pos.right()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Left, robot_pos, Tile::RobotRight)
            } else {
                panic!("No scaffolding to go to");
            }
        }
        Tile::RobotRight => {
            if board.get_tile_result(robot_pos.right()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Forward, robot_pos.right().unwrap(), Tile::RobotRight)
            } else if board.get_tile_result(robot_pos.down()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Right, robot_pos, Tile::RobotDown)
            } else if board.get_tile_result(robot_pos.up()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Left, robot_pos, Tile::RobotUp)
            } else {
                panic!("No scaffolding to go to");
            }
        }
        Tile::RobotLeft => {
            if board.get_tile_result(robot_pos.left()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Forward, robot_pos.left().unwrap(), Tile::RobotLeft)
            } else if board.get_tile_result(robot_pos.up()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Right, robot_pos, Tile::RobotUp)
            } else if board.get_tile_result(robot_pos.down()).unwrap_or(Tile::Empty) == Tile::Scaffold {
                (Movement::Left, robot_pos, Tile::RobotDown)
            } else {
                panic!("No scaffolding to go to");
            }
        }
        _ => {panic!("No robot");}
    }
}

fn get_movement_char(movement: &Movement) -> char {
    match movement {
        Movement::Left => 'L',
        Movement::Right => 'R',
        Movement::Forward => 'F',
        Movement::Backward => 'B',
    }
}

fn problem_1() {
    let board = load_initial_board();
    println!("{}", board);
    let intersections = find_intersections(&board);
    let mut sum = 0;
    for pos in intersections {
        let aligment = get_aligment(&pos);
        sum += aligment;
    }
    println!("sum = {}", sum);
}

fn problem_2a() {
    let mut board = load_initial_board();
    println!("{}", board);
    let mut movement: Movement;
    let (mut robot_pos, mut robot_tile) = find_robot(&board);
    let mut prev_robot_pos = robot_pos;
    let mut movement_chars = String::new();
    for i in 0..343 {
        let out = next_movement(&board, robot_pos, robot_tile);
        movement = out.0;
        robot_pos = out.1;
        robot_tile = out.2;
        board.set_tile(&robot_pos, robot_tile);
        board.set_tile(&prev_robot_pos, Tile::Scaffold);
        prev_robot_pos = robot_pos;
        movement_chars.push(get_movement_char(&movement));
        // println!("i = {}", i);
        // println!("{:?}", movement);
        // println!("{:?}", robot_pos);
        // println!("{:?}", robot_tile);
    }
    println!("{}", movement_chars);
    // println!("{}", board);

    let mut m_chars = Vec::<i32>::new();
    for c in movement_chars.chars() {
        match c {
            'L' => {m_chars.push(0);}
            'R' => {m_chars.push(-1);}
            'F' => {
                let n = m_chars.len();
                let last = m_chars[n-1];
                if last == 0 || last == -1 {
                    m_chars.push(1)
                } else {
                    m_chars[n-1] += 1
                }
            }
            _ => {panic!("Unknown movement");}
        }
    }
    // let substrings = vec!(
    //     vec!(0, 12, 0, 6, 0, 8, -1, 6, 0, 8, 0, 8, -1, 4, -1, 6, -1, 6),
    //     vec!(0, 12, 0, 6, 0, 8, -1, 6, 0, 8, 0, 8, -1, 4, -1, 6, -1, 6),
    // );
    println!("{:?}", m_chars);
}

fn problem_2b() {
    let input = String::from("A,B,A,B,C,C,B,A,B,C\nL,12,L,6,L,8,R,6\nL,8,L,8,R,4,R,6,R,6\nL,12,R,6,L,8\nn\n");
    let board = run_movements(&input);
}

pub fn run() {
    problem_2b();
}
