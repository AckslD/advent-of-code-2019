use std::collections::VecDeque;
use advent2019::intcode::{
    Program,
    ProgramState,
    Memory,
    execute_program
};

fn get_initial_program(day: i64, input: VecDeque<i64>) -> Program {
    let raw_program: Vec<i64>;
    raw_program = vec!(1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,13,1,19,1,9,19,23,2,23,13,27,1,27,9,31,2,31,6,35,1,5,35,39,1,10,39,43,2,43,6,47,1,10,47,51,2,6,51,55,1,5,55,59,1,59,9,63,1,13,63,67,2,6,67,71,1,5,71,75,2,6,75,79,2,79,6,83,1,13,83,87,1,9,87,91,1,9,91,95,1,5,95,99,1,5,99,103,2,13,103,107,1,6,107,111,1,9,111,115,2,6,115,119,1,13,119,123,1,123,6,127,1,127,5,131,2,10,131,135,2,135,10,139,1,13,139,143,1,10,143,147,1,2,147,151,1,6,151,0,99,2,14,0,0);
    let output: Vec<i64> = Vec::new();
    Program{
        memory: Memory{data: raw_program},
        input: input,
        output,
        instr_pointer: 0,
        state: ProgramState::Running,
        rel_base: 0,
    }
}

fn find_noun_and_verb() -> (i64, i64) {
	for noun in 0..100 {
        for verb in 0..100 {
            let input = VecDeque::new();
            let mut program = get_initial_program(2, input);
            program.memory.write(1, noun);
            program.memory.write(2, verb);
            execute_program(&mut program, ProgramState::Finished);
            if program.memory.read(0) == 19690720 {
                return (noun, verb);
            }
        }
    }
	(0, 0)
}

pub fn run() {
    // Problem 1
	// let mut program = get_initial_program(2, VecDeque::new());
	// execute_program(&mut program, ProgramState::Finished);
	// println!("{:?}", program.memory.read(0));

    // Problem 2
    let (noun, verb) = find_noun_and_verb();
    println!("{}", 100 * noun + verb);
}
